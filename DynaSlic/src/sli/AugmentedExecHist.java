/**
   File: src/sli/AugmentedExecHist.java
   Date      		Author      	Changes
   11/29/12		hcai				Created based on src/sli/AugExecHistory.java,  stash key type changed from NodePointEx to DynaNodePoint
   11/30/12		hcai				HashMap->LinkedHashMap to keep the order of items as that of the entrance;
    										also HashSet->LinkedHashSet in rfindNearestNode to ensure checking element by the order of entrance;
    										tested;
    12/01/12		hcai				ensure the entrance order to be kept for quickIndex too;
    										primarily, fix the issue that AEH may MISS some node in the given file due to messy assignment of the 
    										occurrence number to each DynaNodePoint in AEH's stash
*/
package sli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dua.global.dep.DependenceFinder.NodePoint;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

import sli.DynaDependenceFinder.DynaNodePoint;
import sli.DynaDependenceFinder.DynaNodePoint.DynaNodePointComparator;

/** Create a data structure for Augmented Execution History read from a given file, which is the execution output of the subject;
 *   Here the  AEH is defined a sequence of statement occurrences where each element is in the format of
 *  	<Statement Id with occurrence number, {[value of the variable defined in the statement], program counter>
 */
public class AugmentedExecHist
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// embedded class
	public static class NodeRecord {
		public Object v; // value of defined variable
		public DynaNodePoint pc; // program counter
		
		NodeRecord() {
			v = null; // null indicates that this is unknown so far
			pc = null; // null indicates that this is unknown so far
		}
		
		public NodeRecord(Object v, DynaNodePoint pc) { this.v = v; this.pc = pc;}
		@Override public int hashCode() { return pc.hashCode() + v.hashCode(); }
		@Override public boolean equals(Object o) {
			return pc == ((NodeRecord)o).pc && v == ((NodeRecord)o).v; 
		}
		@Override public String toString() { return "{v=" + v + ", pc=" + pc + "}"; }
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// class data section
	private final static String DATA_PREFIX = "|||";
	private final static int DATA_PREFIX_LEN = DATA_PREFIX.length();
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// instance data section
	private String inFile; // file where the history is to be loaded
	private int startId; // if given specifically, will only a partial execution history, e.g. the part relevant to this statement 
	private boolean bStoreValue; // Store values can incur much higher memory consumption
	private Map<DynaNodePoint, NodeRecord> stash; // the storage, a map from NodePoint to AEH record
	private Map<Integer, Integer> quickidx; // a quick indexing map: statement id -> number of occurrences
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// instance routine section
	AugmentedExecHist() { this.inFile = ""; startId= -1; bStoreValue = false; } // -1 for startId means that it will not be used
	AugmentedExecHist(String inFile) {	this.inFile = inFile; bStoreValue = false; startId = -1;}
	AugmentedExecHist(String inFile, int startId) {	this.inFile = inFile; bStoreValue = false; this.startId = startId;}
	public boolean storeValue() { return this.bStoreValue; }
	public void storeValue(boolean bStore) { this.bStoreValue = bStore; }
	public int getStart() { return startId; }
	public Map<DynaNodePoint, NodeRecord> getStash() { return stash; }
	public Map<Integer, Integer> getQuickidx() { return quickidx; }
		
	public int LoadAEH() {
		return LoadAEH(this.inFile);
	}
	public int LoadAEH(String inFile) {
		if (inFile.length() < 1) return -1;
		this.inFile = inFile;
		String infile = this.inFile;
		
		int startId = this.startId;
		boolean reachedStart = (-1 == startId);
		int lStmtCnt = 0;
		DynaNodePoint lastNode = null, curNode = null;
		String val = null;
		Integer nor = null;
		
		// for the purpose of DDG construction, the order of AEH items in which they are inserted into is important, so use LinkedHashMap
		stash = new LinkedHashMap<DynaNodePoint, NodeRecord>();
		
		/** this indexing map, which is just for quick querying the existence of a node, can simply use HashMap, that will place
		 * the entries in a random order, far from being the one in which items are inserted.
		 */
		quickidx = new LinkedHashMap<Integer, Integer>();
		
		// create the inner structure in order to query info for creating NodePoint
		StmtMapper.getCreateInverseMap();
		
		try{
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(infile));
			String s;
			while ((s = reader.readLine()) != null) {
				if((!s.startsWith(DATA_PREFIX)) && (s.indexOf(DATA_PREFIX) != -1)){
					s = s.substring(s.indexOf(DATA_PREFIX));
				}
				// check if line is exec hist output line
				if (!s.startsWith(DATA_PREFIX)) {
					System.out.println("A line has been skipped: " + s);
					continue;
				}
				
				// parse point as integer value that is positive if followed by [1] and negative if followed by [0]
				final int bracketPos = s.indexOf('[', DATA_PREFIX_LEN);
				final int point = Integer.valueOf(s.substring(DATA_PREFIX_LEN, bracketPos));
				if (!reachedStart) {
					if (point == startId)
						reachedStart = true;
					else
						continue; // ignore data occurring BEFORE start point
				}
				final char sign = s.charAt(bracketPos + 1);
				assert sign == '0' || sign == '1';
				//final int signedPoint = sign == '1'? point : -point;
				assert s.charAt(bracketPos + 2) == ']' && s.charAt(bracketPos + 3) == '=';
				
				// store execution history entry point->val
				nor = (Integer) quickidx.get(point);
				if ( null == nor ) {
					nor = 0;
				}
				nor++;
				quickidx.put(point, nor);
				
				curNode = new DynaNodePoint(StmtMapper.getNodeFromGlobalId(point), sign-'0', nor);
				
				if ( lStmtCnt >= 1 ) {
					if ( storeValue() ) {
						// parse value
						val = removeFinalAddress(s.substring(bracketPos + 4));
					}
					else {
						val = null;
					}
					stash.put( lastNode, new NodeRecord(val, curNode) );
				}
				
				lastNode = curNode;
				lStmtCnt ++;
			}
			
			// the last one read in
			stash.put( curNode, new NodeRecord(val, null) );
			
			return 0;
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return -2; 
		}
		catch (IOException e) {
			throw new RuntimeException(e); 
		}
	}
	
	/** reversely search the nearest nodepoint in the AEH that matches the given node target, from the position 
	 *  given by start; if no different node from target can be found, nullify the In-out parameter ret.
	 *  
	 *  the function returns the distance the search has gone through, MAX_VALUE 
	 *  indicates infinite distance  due to failure in finding the target
	 */
	public int rfindNearestNode(int start, NodePoint target, List<DynaNodePoint> ret) {
		int dist = 0;
		//System.out.println("search  target " + target + " from " + start);
		Set<DynaNodePoint> keyset = new LinkedHashSet<DynaNodePoint>(stash.keySet());
		ArrayList<DynaNodePoint> allDynaPnts = new ArrayList<DynaNodePoint>(keyset);
		ListIterator<DynaNodePoint> lsiter = allDynaPnts.listIterator(start);
		
		while ( lsiter.hasPrevious() ) {
			DynaNodePoint curdynapnt = (DynaNodePoint)lsiter.previous();
			dist++;
			
			//System.out.println("current AEH node " + curdynapnt + " versus  target " + target);
			if ( StmtMapper.getGlobalNodeId(curdynapnt.getN()) == StmtMapper.getGlobalNodeId(target.getN()) &&
					curdynapnt.getRhsPos() == target.getRhsPos() ) {
				//System.out.println("Got one!");
				ret.add(curdynapnt);
				return dist;
			}
		}
		return Integer.MAX_VALUE;
	}
	
	private static String removeFinalAddress(String s) {
		// remove object address, if it exists
		final int addrPos = s.lastIndexOf('@');
		if (addrPos != -1) {
			// now, ensure there is an hexadecimal number after the @
			boolean removeAddr = true;
			for (int i = addrPos + 1; i < s.length(); ++i) {
				final char c = s.charAt(i);
				if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'))) {
					removeAddr = false;
					break;
				}
			}
			if (removeAddr) {
				s = s.substring(0, addrPos + 1);
			}
		}
		return s;
	}

	// DEBUG PURPOSES
	public void dumpStash() {
		Set<DynaNodePoint> allDynaPnts = new LinkedHashSet<DynaNodePoint>(stash.keySet());
		System.out.println("======================= All Records in AEH Stash =====================");
		for (DynaNodePoint dynapnt : allDynaPnts) {
			System.out.println("<" + dynapnt + "," + (NodeRecord)stash.get(dynapnt) + ">");
		}
		System.out.println("============================================================");
	}
	
	public void dumpQuickidx() {
		Set<Integer> pnts = new LinkedHashSet<Integer>(quickidx.keySet());
		System.out.println("======================= All Records in AEH Index ====================");
		for (Integer pnt : pnts) {
			System.out.println("(" + pnt + ":" + (Integer)quickidx.get(pnt) + ")");
		}
		System.out.println("===========================================================");
	}
}

// the end
