/**
   File: src/sli/DynaSliOptions.java
   Date      		Author      	Changes
   11/29/12		hcai				Created Copying ProbSliOptions.java from Probsli
   11/30/12		hcai				reconstruct for handling really useful options only; tested				
*/
package sli;

import java.util.ArrayList;
import java.util.List;

public class DynaSliOptions {
	private static List<Integer> startStmtIds = null;
	
	 // the statement id as a part of the slicing criterion, if not specified, the first startStmtId will be used
	private static Integer dynasliId = null;
	
	private static boolean debugOut = false;
	private static int dist = -1; // default distance
	
	// file path, including file name, giving the Augmented execution history
	private static String aehFile = "";
	
	// if do dynamic slicing, will only do AEH instrumentation if not
	private static boolean dynaSlice = false;
		
	public static List<Integer> getStartStmtIds() { return startStmtIds; }
	public static Integer getSlicingId() { return dynasliId; }
	public static boolean debugOut() { return debugOut; }
	public static int dist() { return dist; }
	public static String aehFile() {return aehFile;}
	public static boolean dynaSlice() { return dynaSlice; }
	
	public static String[] process(String[] args) {
		List<String> argsFiltered = new ArrayList<String>();
		
		boolean allowPhantom = true;
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];
			if (arg.startsWith("-start:")) {
				assert startStmtIds == null;
				startStmtIds = dua.util.Util.parseIntList(arg.substring("-start:".length()));
			}
			else if (arg.startsWith("-sliid")) {
				dynasliId = Integer.parseInt(args[++i]);
			}
			else if (arg.equals("-debug"))
				debugOut = true;
			else if (arg.startsWith("-dist")) {
				dist = Integer.parseInt(args[++i]);
				assert dist >= 0;
			}
			else if (arg.startsWith("-aehfile")) {
				aehFile = args[++i];
			}
			else if (arg.equals("-dynaSlice"))
				dynaSlice = true;
			else if (arg.equals("-nophantom"))
				allowPhantom = false;
			else
				argsFiltered.add(arg);
		}
		
		if (allowPhantom)
			argsFiltered.add("-allowphantom");
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return argsFiltered.toArray(arrArgsFilt);
	}
}
