/**
   File: src/sli/DynaSli.java
   Date      		Author      	Changes
   11/29/12		hcai				Created  
   11/30/12		hcai				Do dynamic slicing; Assistive functions like options handling; tested
*/
package sli;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import profile.DynSliceInstrumenter;
import profile.ExecHistInstrumenter;
import dua.Extension;
import dua.Forensics;
import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

import sli.DynaDependenceFinder.DynaDependence;
import sli.DynaDependenceFinder.DynaNodePoint;
import sli.DynaDependenceFinder.DynaNodePoint.DynaNodePointComparator;
import sli.DynaDependenceGraph;
import sli.AugmentedExecHist;

public class DynaSli implements Extension {
	
	public static void main(String[] args) {
		args = preProcessArgs(args);
		
		DynaSli probSlicer = new DynaSli();
		Forensics.registerExtension(probSlicer);
		Forensics.main(args);
	}
	
	private static String[] preProcessArgs(String[] args) {
		args = DynaSliOptions.process(args);
		
		// add option to consider formal parameters as defs and return values as uses, for reaching def/use analysis
		//     and option to not remove repeated branches in switch nodes
		String[] args2 = new String[args.length + 2];
		System.arraycopy(args, 0, args2, 0, args.length);
		args2[args.length]     = "-paramdefuses";
		args2[args.length + 1] = "-keeprepbrs";
		
		args = args2;
		return args;
	}
	
	@Override public void run() {
		System.out.println("Running DynaSli extension of DUA-Forensics");
		
		StmtMapper.getCreateInverseMap();
		// for now, assume exactly one start point is provided
		CFGNode nStart = StmtMapper.getNodeFromGlobalId( DynaSliOptions.getStartStmtIds().get(0) );
		
		// Perform Agrawal's Dynamic Slicing
		if (DynaSliOptions.dynaSlice()){
			System.out.println("Loading augmented execution history from file " + DynaSliOptions.aehFile() + "......");
			// load execution history in the first place
			AugmentedExecHist aeh = new AugmentedExecHist();
			if (0 != aeh.LoadAEH(DynaSliOptions.aehFile())) {
				System.err.println("Can not load the augmented execution history required for building DDG!");
				return;
			}
			//DEBUG
			if (DynaSliOptions.debugOut()) {
				aeh.dumpStash();
				aeh.dumpQuickidx();
			}
			
			System.out.println("Constructing static DependenceGraph from statement " + DynaSliOptions.getStartStmtIds().get(0) +
					"[" + nStart + "] .......");
			DynaDependenceGraph ddg = new DynaDependenceGraph(new NodePoint(nStart, NodePoint.POST_RHS), -1, aeh);
			// if no specific slicing criterion statement id is specified, just use the first start statement id that is specified for
			// the forward static slicing when creating the static PDG
			Integer sliid = DynaSliOptions.getSlicingId();
			if (null == sliid) {
				sliid = DynaSliOptions.getStartStmtIds().get(0);
			}
			
			CFGNode sliNode = StmtMapper.getNodeFromGlobalId(sliid);
			System.out.println("Start dynamic slicing for statement " + sliid + "[" + sliNode + "] .......");
			Set<NodePoint> slicePnts = ddg.getPointsInDynaSlice( new NodePoint(sliNode, NodePoint.POST_RHS) );
			
			// dump the dynamic slice (statement ids only  for now
			// TODO: will update when the depth for each node got computed and ranked
			if (DynaSliOptions.debugOut()) {
				System.out.println("=============== Dynamic Slice for Statement " + sliid + "[" + 
						sliNode + "] ============= ");
				for(NodePoint n:slicePnts) {
					System.out.println(n.toString());
				}
			}
			
			// just print all node points in the dynamic slice computed
			System.out.println("Writing dynamic slice to dynaslice.out .... ");
			List<NodePoint> pntsSorted = new ArrayList<NodePoint>(slicePnts);
			Collections.sort(pntsSorted, NodePoint.NodePointComparator.inst);
			File fOut = new File(dua.util.Util.getCreateBaseOutPath() + "dynaslice.out");
			try {
				Writer writer = new FileWriter(fOut);
				
				Collections.sort(pntsSorted, NodePointComparator.inst);
				for (NodePoint pTgt : pntsSorted) {
					String stmtid = pTgt.toString();
					if (pTgt.getRhsPos() == NodePoint.PRE_RHS) writer.write("-");
					writer.write(stmtid.substring(0, stmtid.indexOf('[')) + "\n");
				}
				
				writer.flush();
				writer.close();
			} catch (IOException e) { e.printStackTrace(); }

			/** this is just a place holder for later considerations for some purposes
			 * but finding nodes with designated distance from the start node is only possible when doing the dynamic slicing, when
			 * the DDG will have been already constructed;
			 */
			if (DynaSliOptions.dist() != -1) {
				findDistPoints(ddg);
			}
			
			System.out.println("DynaSli extension Finished");
			return;
		}
		
		/** 
		 * If this program is started not for dynamic slicing, it should be used for doing the execution history instrumentation;
		 * Otherwise, it will be in vain
		 * 
		 * instrument subject to output augmented execution histories, then run the instrumented subject to get the AEH file
		 * these may be the steps prior to doing the dynamic slicing, which requires the output file holding AEH
		 */
		System.out.println("instrumenting execHistory from statement " + DynaSliOptions.getStartStmtIds().get(0) +
				"[" + nStart + "] .......");
		DependenceGraph depGraph = new DependenceGraph(new NodePoint(nStart, NodePoint.POST_RHS), -1);
		ExecHistInstrumenter instr = new ExecHistInstrumenter();
		instr.instrument(depGraph.getPointsInSlice());
		System.out.println("DynaSli extension Finished");
		
	}
	
	/** Looks for all points at distance d (given by setting). */
	private void findDistPoints(DynaDependenceGraph depGraph) {
		final int d = DynaSliOptions.dist();
		
		Set<DynaNodePoint> pntsAtDist = findNextDistPoints(depGraph, depGraph.getStart(), d);
		List<DynaNodePoint> sortedPntsAtD = new ArrayList<DynaNodePoint>(pntsAtDist);
		Collections.sort(sortedPntsAtD, DynaNodePointComparator.inst);
		
		// DEBUG: print all points at dep distance d
		System.out.println("DISTANCE " + d + " points from " + StmtMapper.getGlobalNodeId(depGraph.getStart().getN()) + ": total " + sortedPntsAtD.size());
		for (DynaNodePoint pntAtD : sortedPntsAtD)
			System.out.println(" " + StmtMapper.getGlobalNodeId(pntAtD.getN()));
	}
	
	/** Recursive helper for findDistPoints */
	private Set<DynaNodePoint> findNextDistPoints(DynaDependenceGraph depGraph, DynaNodePoint pnt, int d) {
		assert d >= 1;
		
		Set<DynaNodePoint> pntsAtDist = new HashSet<DynaNodePoint>();
		if (d == 1) {
			// get immediate successors in dep graph
			for (DynaDependence dep : depGraph.getOutDeps(pnt))
				pntsAtDist.add(dep.getTgt());
		}
		else {
			// for each successor in dep graph, recursively advance with d-1
			for (DynaDependence dep : depGraph.getOutDeps(pnt))
				pntsAtDist.addAll( findNextDistPoints(depGraph, dep.getTgt(), d - 1) );
		}
		
		// DEBUG
		Set<DynaNodePoint> dbgPntsSet = new HashSet<DynaNodePoint>();
		for (DynaDependence dep : depGraph.getOutDeps(pnt))
			dbgPntsSet.add(dep.getTgt());
		List<DynaNodePoint> dbgSortedPnts = new ArrayList<DynaNodePoint>(dbgPntsSet);
		Collections.sort(dbgSortedPnts, DynaNodePointComparator.inst);
		System.out.println(" d " + d + ": " + pnt + " -> " + dbgSortedPnts);
		
		return pntsAtDist;
	}
}

