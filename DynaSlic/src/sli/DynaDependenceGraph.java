/**
   File: src/sli/DynaDependenceGraph.java
   Date      		Author      	Changes
   11/29/12		hcai				Created based on dua.global.dep.DependenceGraph
   11/30/12		hcai				build DDG, debug, tested
   12/01/12		hcai				print var/brid for dependence edges to differentiate multiple edge between
   											two nodes, in dumpDDG;
   											pntToDeps includes all DDG points in its key set too;
   											add more annotations;
*/
package sli;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import soot.Local;
import soot.SootMethod;
import soot.Value;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.Constant;
import soot.jimple.RetStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.Stmt;
import dua.Options;
import dua.global.ProgramFlowGraph;
import dua.global.ReachabilityAnalysis;
import dua.global.ReqBranchAnalysis;
import dua.global.dep.DependenceFinder.ControlDependence;
import dua.global.dep.DependenceFinder.DataDependence;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
//import dua.global.dep.DependenceFinder.Dependence.DepType;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.method.CFG;
import dua.method.CFGDefUses;
import dua.method.CallSite;
import dua.method.MethodTag;
import dua.method.ReachableUsesDefs;
import dua.method.CFG.CFGNode;
import dua.method.CFGDefUses.Branch;
import dua.method.CFGDefUses.CSArgVar;
import dua.method.CFGDefUses.Def;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.ReturnVar;
import dua.method.CFGDefUses.StdVariable;
import dua.method.CFGDefUses.Use;
import dua.method.CFGDefUses.Variable;
import dua.util.Pair;

import sli.AugmentedExecHist.NodeRecord;
import sli.DynaDependenceFinder.DynaControlDependence;
import sli.DynaDependenceFinder.DynaDataDependence;
import sli.DynaDependenceFinder.DynaDependence;
import sli.DynaDependenceFinder.DynaNodePoint;
import sli.DynaDependenceFinder.DynaDependence.DepType;
import sli.DynaDependenceFinder.DynaNodePoint.DynaNodePointComparator;

import dua.global.dep.DependenceGraph;
import fault.StmtMapper;
import sli.AugmentedExecHist;
import sli.DynaSliOptions;

/** extension to the static DependenceGraph, creating DDG with a given augmented execution history
 *  
 *   dynamic slicing will not be automatically done until explicit call to getPointsInSlice(node, var), where
 *   (node,var) gives the slicing criterion and var gives a single variable used at the given node;
 *   if slice for a set of variables is needed, just call this routine multiple times each for a single var, and then
 *   unionize the slices each returned from a single call  corresponding to the slice for that var only  
 */
//public class DynaDependenceGraph extends DependenceGraph {
public class DynaDependenceGraph {
	private static boolean debugOut = false;
	
	/** Start point. */
	private final DynaNodePoint pointStart;
	/** End points. */
	private Set<DynaNodePoint> pointEnds;
	/** Map node->{out-deps} */
	private Map<DynaNodePoint, Set<DynaDependence>> pntToDeps = new HashMap<DynaNodePoint, Set<DynaDependence>>();
	/** Map node->{in-deps} */
	private Map<DynaNodePoint, Set<DynaDependence>> revPntToDeps = new HashMap<DynaNodePoint, Set<DynaDependence>>();
	/** Used during each of the computation phases of the dependence graph to avoid re-visiting points. */
	private Set<DynaNodePoint> visitedPoints = new HashSet<DynaNodePoint>();
	/** Caches all dependences (edges) in graph. */
	private Set<DynaDependence> deps = new HashSet<DynaDependence>();
	
	/** a reversed static dependence graph, e.g. each edge being tgt->src, will make the construction of DDG easier since:
	 *  (1) the current implementation of DependenceGraph assumes that, for any edge src->tgt, tgt is dependent on src
	 *  (2) this code implements the DDG construction using Argrawl's Approach 3 in his "dynamic program slicing" (PLDI90)
	 *  	  paper, where in an edge src->tgt means  that src is dependent on tgt.
	 *  (3) adopting the semantics of "using an edge to represent the dependence between the src and tgt node on the edge" in 
	 *  	  (2)  fits the algorithm of creating DDG by going through the AEH elements   
	 */
	private Map<NodePoint, Set<Dependence>> revPdgPntToDeps = new HashMap<NodePoint, Set<Dependence>>(); 

	public DynaNodePoint getStart() { return pointStart; }
	public Set<DynaNodePoint> getEnds() { return pointEnds; }
	public Set<DynaDependence> getOutDeps(DynaNodePoint p) { return pntToDeps.get(p); }
	public Set<DynaDependence> getInDeps(DynaNodePoint p) { return revPntToDeps.get(p); }
	public Set<DynaDependence> getDeps() { return deps; }
	public DependenceGraph getPdg() { return pdg; }
	public AugmentedExecHist getAeh() { return aeh; }
	
	public void turnDebug(boolean m) { debugOut = m; }
	
	/* retrieve incoming dependence edges for a given node point p in the static PDG */
	public Set<Dependence> getPdgInDeps(NodePoint p) { return revPdgPntToDeps.get(p); }
	
	/* use this embedded child object to manipulate the PDG (static DependenceGraph) */
	private DependenceGraph pdg;

	/* the augmented execution history on which the dynamic dependence graph is to be constructed */
	private AugmentedExecHist aeh;
	
	/*
	 * by default, we would create a FULL, not partial so as to only concern the part affecting pStart, PDG thus DDG;
	 * 
	 * of course, this will also depend on if the given AEH is a full execution history, versus a partial associated with 
	 * those statement affecting pStart only
	 */
	public DynaDependenceGraph(AugmentedExecHist aeh) {
		this(new NodePoint(StmtMapper.getNodeFromGlobalId(0), NodePoint.POST_RHS), -1, aeh);
	}
	
	public DynaDependenceGraph(NodePoint pStart, int dist, AugmentedExecHist aeh) {
		this(pStart, new ArrayList<NodePoint>(), dist, aeh);
	}
	
	/** Builds DDG based on the static DependenceGraph (Presumably the PDG), and the given execution history
	 */
	public DynaDependenceGraph(NodePoint pStart, List<NodePoint> pTargets, int dist, AugmentedExecHist aeh) {
		// create the static DependenceGraph first
		this.pdg = new DependenceGraph(pStart, pTargets, dist);
		// create the reversed PntToDepts map, i.e. tgt->inDepts
		getCreateReversedPdgPntToDepts();
		
		this.aeh = aeh;

		this.pointStart = new DynaNodePoint(pStart.getN(), pStart.getRhsPos());
		List<DynaNodePoint> pdynaTargets = new ArrayList<DynaNodePoint>();
		for (NodePoint pnt : pTargets) {
			pdynaTargets.add(new DynaNodePoint(pnt.getN(), pnt.getRhsPos()));
		}
		this.pointEnds = new HashSet<DynaNodePoint>(pdynaTargets);
		
		if (DynaSliOptions.debugOut()) {
			this.turnDebug(true);
		}
		
		// create all dependencies (data and control) based on the static Dependence Graph (PDG) and given execution history
		doConstruct();
		
		/** a quick summary of the DDG created right now
		   * note: since we create a graph node for each node in the AEH, the size of this DDG equals to the size of 
		   * the AEH used for constructing the DDG
		   * 
		   * pntToDeps and revPntToDeps differ in each maintaining a reversed edge direction of the another, both fully describing
		   * the DDG itself, so both include all DDG points in their respective key set. Double-storage in DDG is only for convenience,
		   * although for the purpose of dynamic slicing using Agrawal's approach 3 does not really use pntToDeps for now. 
		   * 		-> this is different from DependenceGraph where the only storage pntToDeps keeps the points with at least one out-going
		   * 			edge in its key set.    
		   */
		System.out.println("DDG profile after construction: # deps " + deps.size() + " # points " + pntToDeps.keySet().size());

		if (debugOut) {
			// DEBUG - dump all content of the DDG
			dumpDDG();
		}
	}
	
	public void dumpDDG() {
		List<DynaNodePoint> srcs = new ArrayList<DynaNodePoint>(pntToDeps.keySet());
		System.out.println("=============== All Dependence edges in DDG ============= ");
		for (DynaNodePoint src:srcs) {
			if (pntToDeps.get(src).isEmpty()) continue;
			System.out.print("" + src + " --> ");
			int i = 0;
			for (DynaDependence dep:pntToDeps.get(src)) {
				if ( i > 0 ) System.out.print(",");
				System.out.print("" + dep.getTgt());
				
				if (dep instanceof DynaDataDependence) System.out.print(":(d,var=" + ((DynaDataDependence)dep).getVar() + ")");
				else System.out.print(":(c,brid=" + ((DynaControlDependence)dep).getBrId() + ")");
				
				i++;
			}
			System.out.println("");
		}
	}
	
	public void printFwdSlice() {
		System.out.print('s'); // indicates start point
		printFwdSliceHelper(pointStart, "", new HashSet<DynaNodePoint>());
		System.out.println();
	}
	
	private void printFwdSliceHelper(DynaNodePoint p, String prefix, Set<DynaNodePoint> visited) {
		if (!visited.add(p)) {
			System.out.print("@" + p);
			return;
		}
		
		final String sP = p + "->";
		System.out.print(sP);
		Set<DynaDependence> outDeps = pntToDeps.get(p);
		/**
		 * the given Start point might not be in the DDG, when, for instance, it has never been executed according to the
		 * given execution history
		 */
		if (null == outDeps || outDeps.isEmpty())
			System.out.print("END");
		else {
			List<DynaNodePoint> pSuccs = new ArrayList<DynaNodePoint>();
			Map<DynaNodePoint,Integer> pSuccToDepClass = new HashMap<DynaNodePoint, Integer>(); // 0 = ctrl only, 1 = data only, 2 = both (rare case, but possible)
			for (DynaDependence dep : outDeps) {
				DynaNodePoint pTgt = dep.getTgt();
				pSuccs.add(pTgt);
				final boolean isDataDep = dep instanceof DynaDataDependence;
				
				// decide dep type -- 0 = ctrl only, 1 = data only, 2 = both (rare case, but possible)
				Integer clsOld = pSuccToDepClass.get(pTgt);
				Integer clsNew;
				if (clsOld == null)
					clsNew = isDataDep? 1 : 0;
				else if (clsOld == 0)
					clsNew = isDataDep? 2 : 0;
				else if (clsOld == 1)
					clsNew = isDataDep? 1 : 2;
				else {
					assert clsOld == 2;
					clsNew = 2;
				}
				pSuccToDepClass.put(pTgt, clsNew);
			}
			Collections.sort(pSuccs, DynaNodePointComparator.inst);
			
			// create next prefix with empty spaces matching length of last arrow printed
			char[] nextPreffixChars = new char[prefix.length() + sP.length() + 1]; // extra 1 for c/d/b dep class indicator
			Arrays.fill(nextPreffixChars, ' ');
			String nextPrefix = new String(nextPreffixChars);
			boolean first = true;
			for (DynaNodePoint pSucc : pSuccs) {
				if (first)
					first = false;
				else
					System.out.print("\n" + nextPrefix);
				final int depCls = pSuccToDepClass.get(pSucc);
				System.out.print((depCls == 0)? 'c' : (depCls == 1)? 'd' : 'b');
				printFwdSliceHelper(pSucc, nextPrefix, visited);
			}
		}
	}
	
	private void getCreateReversedPdgPntToDepts() {
		Set<Dependence> alldeps = this.pdg.getDeps();
		for (Dependence dep:alldeps) {
			NodePoint tgt = dep.getTgt();
			Set<Dependence> indeps = revPdgPntToDeps.get(tgt);
			if (null == indeps) {
				indeps = new HashSet<Dependence>();
			}
			indeps.add(dep);
			revPdgPntToDeps.put(tgt, indeps);
		}
	}
	
	private void doConstruct() {
		///////////////////////////////////////
		/**
		 * go through the AEH in the order the NodeRecords were traced, creating nodes for each AEH record that represents
		 * a statement occurrence; Then, adding dependence edges by referring to both the static PDG and the whole AEH 
		 * image   
		 */
		Map<DynaNodePoint, NodeRecord> aehStash = this.aeh.getStash();
		Set<DynaNodePoint> allDynaPnts = new LinkedHashSet<DynaNodePoint>(aehStash.keySet());
		int curpos = 0; // integer position of the node point being dealt with in the AEH
		for (DynaNodePoint dynapnt : allDynaPnts) {
			NodePoint pnt = new NodePoint(dynapnt.getN(), dynapnt.getRhsPos());
			Set<Dependence> indeps = getPdgInDeps(pnt);
			
			// for each statement occurrence, we create a node in the DDG
			this.revPntToDeps.put(dynapnt, new HashSet<DynaDependence>());
			this.pntToDeps.put(dynapnt, new HashSet<DynaDependence>());
			
			// 1. handle control dependency
			int numCDEP = 0;
			for (Dependence dep:indeps) {
				/* for all control dependencies, always add the corresponding edges to the DDG - i.e. no edge will be discarded
				 * because: different occurrences of a same statement should not have different control dependences, although 
				 * the conclusion will be the opposite for data dependences.
				 * 
				 * for a given node in the PDG, the one corresponding to the dynapnt (dismissing the order of ocurrence), 
				 * say,	there should be no more than one control dependence edge;
				 * 
				 * when deciding the DynaNodePoint for the dependence edge to be added to DDG, simply choose the 
				 * latest occurrence of the source node, which has already been executed so far,
				 * on the corresponding dependence edge in the static PDG.				 	
				 */
				if ( dep instanceof ControlDependence) {
					ControlDependence cdep = (ControlDependence)dep;
					DynaNodePoint dynaSrcPnt = null;
					
					List<DynaNodePoint> __agent = new ArrayList<DynaNodePoint>();
					this.aeh.rfindNearestNode(curpos, cdep.getSrc(), __agent);
					if (__agent.size() >= 1)	dynaSrcPnt = __agent.get(0);
					
					/** if would be possible if a node n has been executed while its decision vertex (node) has not yet, 
					 *   a typical example is that the decision vertex is inside a loop where n is the predicate node   
					 */
					// assert null != dynaSrcPnt;
					// if this dependence edge has not yet been "activated", just skip it for now
					if (null == dynaSrcPnt) {
						if (DynaSliOptions.debugOut()) {
							System.out.println("ControlDep edge not activated: Not find statement " + 
									cdep.getSrc() + " 's latest occurrence in AEH before " + dynapnt);
							
							System.out.println("One ControlDependence edge for " + pnt + "  has been dealt with: " + cdep);
						}
						numCDEP ++;
						//indeps.remove(dep);
						continue;
					}
					
					createControlDep(dynaSrcPnt, cdep.getBrId(), dynapnt);
					
					numCDEP ++;
					
					//indeps.remove(dep);
				}
				else {
					assert dep instanceof DataDependence;
				}
			}
			
			/** if would be weird if we found more than ONE control dependence edge in the static PDG for a given NodePoint
			 *   inside a procedure; but for interprocedural PDG, this is possible;
			 */
			//assert numCDEP <= 1;
			
			// 2. deal with data dependencies
			/* Note: 
			 * (1) for each variable used on a DDG node n, there is at most one data dependence edge from an another node 
			 *      it depends on (i.e. the node defining this variable) to n;
			 * (2) self-loop edge can happen to be in the static PDG, but must NOT in the DDG - representing each
			 *     occurrence of a statement with a separate graph node should have already eliminate those self-loops
			 * 
			 * So, for data dependencies, which dependence edges will be added is decided by the following rule
			 * for each of the variables used in current DynaNodePoint, find the DynaNodePoint in 
			 * the AEH that corresponds to the nearest reaching definition of the variable. And, then, create a dependence
			 * edge from the defNode to the current node  
			 */
			
			// go through all used variables
			for (Variable vUse : ((NodeDefUses)pnt.getN()).getUsedVars()) {
				// check all reaching definitions for current variable vUse
				List<Pair<Def,Integer>> defsForUse = DynaDependenceFinder.getAllDefsForUse(vUse, dynapnt.getN());
				int minDist = Integer.MAX_VALUE;
				DynaNodePoint srcnode = null;
				Variable vDef = null;
				for (Pair<Def,Integer> __defnode: defsForUse ) {
					// get one of the NodePoint that defines vUse 
					NodePoint defnode = new NodePoint( __defnode.first().getN(), __defnode.second() );
					
					if ( null ==  __defnode.first().getN().getStmt() ) {
						if (DynaSliOptions.debugOut()) {
							System.out.println("Got a special CFGNode - null statement, the defining Node " +
									__defnode + " of variable " + vUse + " at " + pnt + "; skipped it.");
						}
						continue;
					}
					
					// get the latest occurrence of such node in the partition of AEH we have gone through so far
					DynaNodePoint curSrc = null;
					List<DynaNodePoint> __agent = new ArrayList<DynaNodePoint>();
					int dist = this.aeh.rfindNearestNode(curpos, defnode, __agent);
					if (__agent.size() >= 1)	curSrc = __agent.get(0);
					
					/** if would be possible if a node n using a variable vUse has been executed 
					   * while the node defining vUse has not yet;
					   * A typical case is that the defining node is inside a loop where n is the predicate node
					   */ 
					// assert null != curSrc && Integer.MAX_VALUE != dist;
					
					// if this dependence edge has not yet been "activated", just skip it for now
					if (null == curSrc) {
						if (DynaSliOptions.debugOut()) {
							System.out.println("DataDep edge not activated: Not find statement " + 	
								defnode + " 's latest occurrence in AEH before " + dynapnt);
						}
						continue;
					}
					
					// we only want the nearest definition, while others, including the data dependence edges (from 
					// defnode to pnt), will be discarded from the DDG
					if (dist < minDist) {
						minDist = dist;
						srcnode = curSrc;
						vDef = __defnode.first().getVar();
					}
				}
				
				// no any Defining node for vUse has been executed so far, just skip this variable and check the next used variable
				if (null == srcnode) {
					continue;
				}
				
				// find the dependence corresponding to the edge (nearest defnode->pnt)
				int nFound = 0;
				for (Dependence dep:indeps) {
					if ( ! (dep instanceof DataDependence) ) continue;  // now only dataDependence edges are left to check in indeps
					DataDependence ddep = (DataDependence)dep;
					
					if (DynaSliOptions.debugOut()) {
						System.out.println("Check if " + pnt + " is the endpoint of current edge " + ddep);
					}
					
					// ensure that we have reached the dependence edge being dealt with
					assert shallowEquals(pnt, ddep.getTgt()); 
					
					NodePoint pdgsrcnode = new NodePoint(srcnode.getN(), srcnode.getRhsPos()); 
					if (shallowEquals(pdgsrcnode, ddep.getSrc())) {
						//assert ddep.getVar() == vDef; // the defined variable should match too
						//assert ddep.getVar().mayEqualAndAlias(vDef); // the defined variable should match too
						if (! ddep.getVar().mayEqualAndAlias(vDef)) { 
							if (DynaSliOptions.debugOut()) {
								System.out.println("on the shortest edge from  " + srcnode + " to " + dynapnt + " , " + ddep +
										",  the defined variable " + ddep.getVar() + " on the edge does NOT match the var " + vDef + 
										" on its source node");
							}
							continue;
						}
						
						// now create a data dependence edge from srcnode to dynapnt
						DepType depType = DepType2DynaDepType(ddep.getType());
						createDataDep(srcnode, dynapnt, ddep.getVar(), depType);
						
						if (DynaSliOptions.debugOut()) {
							System.out.println("the shortest edge from  " + srcnode + " to " + dynapnt + " found: " + ddep);
						}
						nFound ++;
					}
				}
				
				/** it is possible that we would find more than ONE dependence edge corresponding to (pdgsrcnode->pnt)
				 *   in the static PDG - each edge for a single variable used at "pnt" that is defined at "pdgsrcnode". Note that
				 *   a single node can define more than one variable!
				 *   
				 *   However, we need to add one of those edges only for constructing the DDG, After all, a single edge is enough to 
				 *   show that one node's connection to another node; DDG is not a superset of PDG, so no need to include the detailed 
				 *   information at the variable level -- only node level relationship is sufficient
				 *   
				 *   nFound can be 0 if (pdgsrcnode->pnt) matches a control dependece edge in the static PDG
				 */
				assert 1 >= nFound;
			}
			curpos ++;
		}
		//
		/////////////////////////////////////
	}
	
	/** Inline method that judges if two NodePoints equal just by the enclosing statement's global Id and rhsPos
	 *  rather than the whole CFGNode
	 */
	public static boolean shallowEquals(NodePoint n1, NodePoint n2)
	{
		//StmtMapper.getCreateInverseMap();
		return StmtMapper.getGlobalNodeId(n1.getN()) == StmtMapper.getGlobalNodeId(n2.getN())  &&
				n1.getRhsPos() == n2.getRhsPos();
	}
		
	/** Helper method that creates and stores data dependencies in corresponding set and map.
	 *  @param var Identifies the defined variable in the dependence; for actual-formal links, it's the argument var. 
	 */
	private void createDataDep(DynaNodePoint pntDef, DynaNodePoint pntUse, Variable var, DepType depType) {
		DynaDependence dynadep = new DynaDataDependence(pntDef, pntUse, var, depType);
		// update the Node->{out-deps} map
		Set<DynaDependence> __outdeps = this.pntToDeps.get(pntDef);
		if (null == __outdeps) {
			__outdeps = new HashSet<DynaDependence>();
		}
		__outdeps.add(dynadep);
		this.pntToDeps.put(pntDef, __outdeps);
		
		// update the Node->{in-deps} map
		Set<DynaDependence> __indeps = this.revPntToDeps.get(pntUse);
		if (null == __indeps) {
			__indeps = new HashSet<DynaDependence>();
		}
		__indeps.add(dynadep);
		this.revPntToDeps.put(pntUse, __indeps);
		
		// update the dependence edge store
		this.deps.add(dynadep);
	}
	
	/** Helper method that creates and stores control dependencies in corresponding set and map 
	 */
	private void createControlDep(DynaNodePoint pntSrc, int brId, DynaNodePoint pntTgt) {
		DynaDependence dynadep = new DynaControlDependence(	pntSrc, brId, pntTgt);
		
		// update the Node->{out-deps} map
		Set<DynaDependence> __outdeps = this.pntToDeps.get(pntSrc);
		if (null == __outdeps) {
			__outdeps = new HashSet<DynaDependence>();
		}
		__outdeps.add(dynadep);
		this.pntToDeps.put(pntSrc, __outdeps);
		
		// update the Node->{in-deps} map
		Set<DynaDependence> __indeps = this.revPntToDeps.get(pntTgt);
		if (null == __indeps) {
			__indeps = new HashSet<DynaDependence>();
		}
		__indeps.add(dynadep);
		this.revPntToDeps.put(pntTgt, __indeps);
		
		// update the dependence edge store
		this.deps.add(dynadep);
	}
	
	/** REACHABILITY analysis should be used instead.
	 * 
	 * @param n1
	 * @param n2
	 * @return
	 */
	private static boolean isFwdReachable(CFGNode n1, CFGNode n2) {
		CFG cfg1 = ProgramFlowGraph.inst().getContainingCFG(n1);
		CFG cfg2 = ProgramFlowGraph.inst().getContainingCFG(n2);
		
		// first, look intraprocedurally; they might be in same cfg, but n2 must be reachable from n1 within that cfg
		if (cfg1 == cfg2 && ReachabilityAnalysis.reachesFromBottom(n1, n2, false))
			return true;
		
		// then, look interprocedurally in (transitive) callees ONLY from callsites at cfg1 intraproc-reachable from n1
		Set<CFG> cfgsFwd = new HashSet<CFG>();
		for (CallSite cs : cfg1.getCallSites()) {
			if (!cs.hasAppCallees())
				continue;
			CFGNode nCS = ProgramFlowGraph.inst().getNode(cs.getLoc().getStmt());
			if (ReachabilityAnalysis.reachesFromBottom(n1, nCS, false))
				for (SootMethod mTgt : cs.getAppCallees())
					cfgsFwd.add(ProgramFlowGraph.inst().getCFG(mTgt));
		}
		
		Set<CFG> workset = new HashSet<CFG>(cfgsFwd);
		while (!workset.isEmpty()) {
			CFG cfg = workset.iterator().next();
			workset.remove(cfg);
			cfgsFwd.add(cfg);
			
			for (CFG cfgSucc : cfg.getCallgraphSuccs())
				if (!cfgsFwd.contains(cfgSucc))
					workset.add(cfgSucc);
		}
		
		return cfgsFwd.contains(cfg2);
	}
	
	private static DynaDependence.DepType DepType2DynaDepType(Dependence.DepType dt) {
		DynaDependence.DepType ret = DepType.INTER;
		switch (dt) {
		case INTER:
			ret = DepType.INTER;
			break;
		case INTRA:
			ret = DepType.INTRA;
			break;
		case FWD_LINK:
			ret = DepType.FWD_LINK;
			break;
		case BACK_LINK:
			ret = DepType.BACK_LINK;
			break;
		}
		return ret;
	}
	
	/** REACHABILITY analysis should be used instead.
	 * 
	 * @param n1
	 * @param n2
	 * @return
	 */
	private static boolean isBackFwdReachable(CFGNode n1, CFGNode n2) {
		CFG cfg1 = ProgramFlowGraph.inst().getContainingCFG(n1);
		CFG cfg2 = ProgramFlowGraph.inst().getContainingCFG(n2);
		
		// look intraprocedurally only in cfg1, NOT forward from cfg1!
		if (cfg1 == cfg2 && ReachabilityAnalysis.reachesFromBottom(n1, n2, false))
			return true;
		
		Set<CallSite> backCSs = new HashSet<CallSite>();
		Set<CallSite> workset = new HashSet<CallSite>(cfg1.getCallerSites());
		while (!workset.isEmpty()) {
			CallSite cs = workset.iterator().next();
			CFGNode nCS = ProgramFlowGraph.inst().getNode(cs.getLoc().getStmt());
			workset.remove(cs);
			backCSs.add(cs);
			
			if (isFwdReachable(nCS, n2))
				return true;
			
			CFG cfg = ProgramFlowGraph.inst().getContainingCFG(nCS);
			for (CallSite csBack : cfg.getCallerSites())
				if (!backCSs.contains(csBack))
					workset.add(csBack);
		}
		
		return false;
	}
	
	/** Whether Two DDG have any node in common. 
	 * 
	 * @param depGraph2
	 * @return
	 */
	public boolean intersects(DynaDependenceGraph depGraph2) {
		// gather collection of affected nodes for this graph
		Set<CFGNode> pointsHere = new HashSet<CFGNode>();
		for (DynaDependence dep : deps)
			pointsHere.add(dep.getTgt().getN());
		
		for (DynaDependence depOther : depGraph2.deps)
			if (pointsHere.contains(depOther.getTgt().getN()))
				return true;
		return false;
	}
	
	/** get all forwardly reachable node in the DDG
	 * 
	 * @param start
	 * @param store
	 */
	public void getForwardReachableNodes(DynaNodePoint start, Set<DynaNodePoint> store) {
		getForwardReachableNodesHelper(start, store, new HashSet<DynaNodePoint>());
	}
	private void getForwardReachableNodesHelper(DynaNodePoint start, Set<DynaNodePoint> store, Set<DynaNodePoint> visited) {
		if (!visited.add(start)) {
			return;
		}
		
		Set<DynaDependence> outdeps = this.getOutDeps(start);
		// recursively search in a depth-first way
		for (DynaDependence dep:outdeps) {
			store.add(dep.getTgt());
			getForwardReachableNodes(dep.getTgt(), store);
		}
	}
	
	/** get all backwardly reachable node in the DDG, e.g. all nodes in DDG that can reach the given node end
	 *  
	 * @param end
	 * @param store
	 */
	public void getBackwardReachableNodes(DynaNodePoint end, Set<DynaNodePoint> store) {
		getBackwardReachableNodesHelper(end, store, new HashSet<DynaNodePoint>());
	}
	private void getBackwardReachableNodesHelper(DynaNodePoint end, Set<DynaNodePoint> store, Set<DynaNodePoint> visited) {
		if (!visited.add(end)) {
			return;
		}
		
		Set<DynaDependence> outdeps = this.getInDeps(end);
		// recursively search in a depth-first way
		for (DynaDependence dep:outdeps) {
			store.add(dep.getSrc());
			getBackwardReachableNodesHelper(dep.getSrc(), store, visited);
		}
	}
	
	public Set<DynaNodePoint> getPointsInDDG() {
		// start with src pnts of deps in slice
		Set<DynaNodePoint> allDDGPoints = new HashSet<DynaNodePoint>(pntToDeps.keySet());
		// add tgt pnts of deps out of those src pnts
		for (Set<DynaDependence> outDeps : pntToDeps.values())
			for (DynaDependence outDep : outDeps)
				allDDGPoints.add(outDep.getTgt());
		return allDDGPoints;
	}
	
	/** return the set of points in the forward static slice */ 
	public Set<NodePoint> getPointsInSlice() {
		return this.pdg.getPointsInSlice();
	}
	
	/** Do the dynamic slicing based on the DDG created before and return the original DynaNodePoints in the slice
	 * i.e. the slice consisting of DynaNodePoints rather than NodePoints - by occurrence of statement instead of statement.   
	 */
	public Set<DynaNodePoint> getDynaSlice(NodePoint i, Variable v) {
		Set<DynaNodePoint> ret = new HashSet<DynaNodePoint>();
		// (1). find the DynaNodePoint in the DDG corresponding to the last definition of v that affected inputs at i in the AEH
		// a. locate the latest occurrence of i in AEH
		Map<DynaNodePoint, NodeRecord> aehStash = this.aeh.getStash();
		DynaNodePoint tgtdynapnt = null; 
		
		List<DynaNodePoint> __agent = new ArrayList<DynaNodePoint>();
		int dist = this.aeh.rfindNearestNode(aehStash.size(), i, __agent);
		if (__agent.size() >= 1)	tgtdynapnt = __agent.get(0);
		
		if ( null == tgtdynapnt ) {
			// the statement in the given criterion has never been executed, no need to proceed further for now., just return
			// an empty slice
			return ret;
		}
		
		// b. search backwards, from tgtdynapnt, in the AEH for the nearest occurrence of the statement that latest defines v
		ArrayList<DynaNodePoint> allDynaPnts = new ArrayList<DynaNodePoint>(aehStash.keySet());
		ListIterator<DynaNodePoint> lsiter = allDynaPnts.listIterator(aehStash.size() - dist);
		DynaNodePoint curdynapnt = null;
		while ( lsiter.hasPrevious() ) {
			curdynapnt = (DynaNodePoint)lsiter.previous();
			List<Variable> vDefs = ((NodeDefUses)curdynapnt.getN()).getDefinedVars();
			if (vDefs.contains(v)) {
				// found, get out
				break;
			}
		}
		// if the given v, possibly mistyped in the caller, is never found defined among all statements that have been executed (i.e. in 
		// the AEH), no way to proceed either
		if ( null == curdynapnt ) {
			return ret;
		}
		// up to here, curdynapnt is the node in the DDG that last defines v in the AEH before the last occurrence of i is executed 
				
		// (2). find all other DynaNodePoints reachable TO the last last definition DynaNodePoint found in (1)
		getBackwardReachableNodes(curdynapnt, ret);
		return ret;
	}
	
	/** Do the dynamic slicing based on the DDG created before and return the NodePoints in the slice
	 * i.e. the slice consisting of NodePoints - by statement only, with all DynaNodePoints representing occurrences of 
	 * a same NodePoint merged into a single node   
	 */
	public Set<NodePoint> getPointsInDynaSlice(NodePoint i, Variable v) {
		Set<NodePoint> ret = new HashSet<NodePoint>();
		Set<DynaNodePoint> dynaSet = getDynaSlice(i,v);
		
		// merge occurrences of statement
		for (DynaNodePoint dynapnt:dynaSet) {
			ret.add(new NodePoint(dynapnt.getN(), dynapnt.getRhsPos()));
		}
		
		return ret;
	}
	
	// a more general version of dynamic slicing : get dynamic slice for a general form of slicing criterion
	public Set<NodePoint> getPointsInDynaSlice(NodePoint i, List<Variable> vars) {
		Set<NodePoint> ret = new HashSet<NodePoint>();
		for(Variable v:vars) {
			ret.addAll(getPointsInDynaSlice(i, v));
		}
		return ret;
	}
	
	// a handy version: given the statement of the criterion only;
	// all used variables at i will be taken into account
	public Set<NodePoint> getPointsInDynaSlice(NodePoint i) {
		return getPointsInDynaSlice(i, ((NodeDefUses)i.getN()).getUsedVars());
	}
	
	// TODO
	/*
	 * Compute depth (distance from the given starting node) for all nodes in DDG, in order to rank them that way
	 */
}

// end
