package profile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import soot.Body;
import soot.Local;
import soot.PatchingChain;
import soot.PrimType;
import soot.RefLikeType;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.Constant;
import soot.jimple.FieldRef;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.IntConstant;
import soot.jimple.Jimple;
import soot.jimple.LookupSwitchStmt;
import soot.jimple.NewArrayExpr;
import soot.jimple.NewExpr;
import soot.jimple.RetStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.TableSwitchStmt;
import soot.util.Chain;
import dua.global.ProgramFlowGraph;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.method.CFGDefUses;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.ObjVariable;
import dua.method.CFGDefUses.Use;
import dua.method.CFGDefUses.Variable;
import dua.method.ReachableUsesDefs.NodeReachDefsUses;
import dua.util.Pair;
import dua.util.Util;
import fault.StmtMapper;

/** Instruments statements to track their augmented execution histories (i.e., exec histories with computed values). */
public class ExecHistInstrumenter {
	private SootClass clsObject;
	private SootClass clsReporter;
	private SootMethod mReportStmt;
	
	public ExecHistInstrumenter() {
		clsObject = Scene.v().getSootClass("java.lang.Object");
		clsReporter = Scene.v().getSootClass("profile.ExecHistReporter");
		// params: sId, pre/post [0|1], objDefVal  -- NOTE: if more than 1 def, there will be 1 reporter call per def
		mReportStmt = clsReporter.getMethod("void reportDef(int,boolean,java.lang.Object)");
	}
	
	public void instrument(Collection<NodePoint> pnts) {
		List<NodePoint> pntsSorted = new ArrayList<NodePoint>(pnts);
		Collections.sort(pntsSorted, NodePoint.NodePointComparator.inst);
		
		// DEBUG
		Set<Integer> bads = new HashSet<Integer>(); //Arrays.asList(
//				3741, 3744));
		System.out.println("Instrumenting points: " + pntsSorted);
		
		for (NodePoint pntDef : pntsSorted) {
			// instrument defined values for this point
			final boolean preRhs = pntDef.getRhsPos() == NodePoint.PRE_RHS;
			NodeDefUses nDef = (NodeDefUses) pntDef.getN();
			Stmt sDef = nDef.getStmt();
			List<Variable> defVars = nDef.getDefinedVars();
			
			final int sId = StmtMapper.getGlobalNodeId(nDef);
			
			// DEBUG
			if (bads.contains(sId))
				continue;
			
			SootMethod mDef = ProgramFlowGraph.inst().getContainingMethod(sDef);
			Body bDef = mDef.retrieveActiveBody();
			PatchingChain pchain = bDef.getUnits();
			
			if (nDef.getSuccs().size() > 1) {  // predicate case -- `defines' value of program counter
				assert !preRhs;
				
				if (sDef instanceof IfStmt) {
					// instrument each outgoing branch with sId and outcome's integer value (e.g., 0=false, 1=true)
					assert nDef.getOutBranches().size() == 2;
					
					// false branch (0)
					List probeF = new ArrayList();
					List argsF = new ArrayList();
					argsF.add(IntConstant.v(sId)); // param 1: node id
					argsF.add(IntConstant.v(preRhs? 0 : 1)); // param 2: pre position at node -- should be always post for conditional statements
					Local lBoxedF = UtilInstrum.getCreateLocal(bDef, LOCAL_OBJ, clsObject.getType());
					addCopyBoxOrCastStmt(bDef, probeF, lBoxedF, IntConstant.v(0));
					argsF.add(lBoxedF); // param 3: var value -- 0 for false branch
					Stmt sReportCallF = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(mReportStmt.makeRef(), argsF) );
					probeF.add(sReportCallF);
					InstrumManager.v().insertProbeAt(nDef.getOutBranches().get(0), probeF);
					
					// true branch (1)
					List probeT = new ArrayList();
					List argsT = new ArrayList();
					argsT.add(IntConstant.v(sId)); // param 1: node id
					argsT.add(IntConstant.v(preRhs? 0 : 1)); // param 2: pre position at node -- should be always post for conditional statements
					Local lBoxedT = UtilInstrum.getCreateLocal(bDef, LOCAL_OBJ, clsObject.getType());
					addCopyBoxOrCastStmt(bDef, probeT, lBoxedT, IntConstant.v(1));
					argsT.add(lBoxedT); // param 3: var value -- 1 for true branch
					Stmt sReportCallT = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(mReportStmt.makeRef(), argsT) );
					probeT.add(sReportCallT);
					InstrumManager.v().insertProbeAt(nDef.getOutBranches().get(1), probeT);
				}
				else {  // switch (table or lookup): insert report of deciding value before switch
					// get value that decides switch-branch to take
					Value valDeciding;
					if (sDef instanceof LookupSwitchStmt)
						valDeciding = ((LookupSwitchStmt)sDef).getKey();
					else {
						assert sDef instanceof TableSwitchStmt;
						valDeciding = ((TableSwitchStmt)sDef).getKey();
					}
					
					// insert probe before switch stmt with boxed deciding value as argument
					List probe = new ArrayList();
					List args = new ArrayList();
					args.add(IntConstant.v(sId)); // param 1: node id
					args.add(IntConstant.v(preRhs? 0 : 1)); // param 2: pre position at node -- should be always post for conditional statements
					Local lBoxed = UtilInstrum.getCreateLocal(bDef, LOCAL_OBJ, clsObject.getType());
					addCopyBoxOrCastStmt(bDef, probe, lBoxed, valDeciding);
					args.add(lBoxed); // param 3: var value -- should be a boxed local primitive (integer-like)
					Stmt sReportCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(mReportStmt.makeRef(), args) );
					probe.add(sReportCall);
					InstrumManager.v().insertAtProbeBottom(pchain, probe, sDef);
				}
			}
			else {  // variable definition(s) case
				for (Variable vDef : defVars) {
					// spare special cases
					if (vDef.isObject() && vDef.getValue() instanceof StaticInvokeExpr)
						continue;
					if (vDef.isArrayRef() && sDef instanceof AssignStmt && ((AssignStmt)sDef).getRightOp() instanceof NewArrayExpr)
						continue;
					List probe = new ArrayList();
					List args = new ArrayList();
					args.add(IntConstant.v(sId)); // param 1: node id
					args.add(IntConstant.v(preRhs? 0 : 1)); // param 2: pre position at node
					args.add(boxVar(mDef, vDef, probe)); // param 3: var value -- modifies probe with boxing code while getting local ref pointing to boxed value
					Stmt sReportCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(mReportStmt.makeRef(), args) );
					probe.add(sReportCall);
					if (preRhs) {
						// any variable defined that is no the lhs (to which the return value is assigned)
						assert !(sDef instanceof IdentityStmt);
						if (!(sDef instanceof AssignStmt) || ((Local)((AssignStmt)sDef).getLeftOp()) != vDef.getValue())
							InstrumManager.v().insertAtProbeBottom(pchain, probe, getAfterSpecialInvokeStmt(pchain, sDef));
					}
					else {
						if (sDef instanceof IdentityStmt) {
							assert defVars.size() == 1;
							if (!isParUsed((IdentityStmt)sDef, (NodeReachDefsUses) nDef, mDef))
								continue;
							
							// just before first non-id stmt (first valid insertion location)
							InstrumManager.v().insertBeforeNoRedirect(pchain, probe, getFirstSafeNonIdStmt(vDef.getValue(), mDef));
						}
						else {
							if (sDef instanceof ReturnStmt)
								InstrumManager.v().insertAtProbeBottom(pchain, probe, sDef); // return stmt
							else {
								assert !(sDef instanceof RetStmt);
								
								// *** TODO: *** this definition should still be taken into account!!
								//   special case: writing of reference to parent (arg 1) in nested class ctor
								if (mDef.getName().equals("<init>") && vDef.isFieldRef()) {
									final String fldName = ((FieldRef)vDef.getValue()).getFieldRef().name();
									if (fldName.equals("this$0"))
										continue; // skip
								}
								// special case: writing of static 'class' field in <clinit>
								if (mDef.getName().equals("<clinit>") && vDef.isFieldRef()) {
									final String fldName = ((FieldRef)vDef.getValue()).getFieldRef().name();
									if (fldName.equals("class$0"))
										continue; // skip
								}
								
								InstrumManager.v().insertBeforeNoRedirect(pchain, probe, getSuccAfterNextSpecialInvokeStmt(pchain, sDef)); // after definition of lhs
							}
						}
					}
				}
			}
		}
	}
	private static Stmt getAfterSpecialInvokeStmt(PatchingChain pchain, Stmt s) {
		return (s.containsInvokeExpr() && s.getInvokeExpr() instanceof SpecialInvokeExpr)? (Stmt)pchain.getSuccOf(s) : s;
	}
	private static Stmt getSuccAfterNextSpecialInvokeStmt(PatchingChain pchain, Stmt s) {
		// if s is new expr, then get past <init> specialinvoke
		if (s instanceof AssignStmt && ((AssignStmt)s).getRightOp() instanceof NewExpr) {  // exclude newarray case
			Local lNew = (Local) ((AssignStmt)s).getLeftOp();
			do { s = (Stmt)pchain.getSuccOf(s); } // get past new expr
			while (!(s.containsInvokeExpr() && s.getInvokeExpr() instanceof SpecialInvokeExpr && ((SpecialInvokeExpr)s.getInvokeExpr()).getBase() == lNew));
		}
		return (Stmt)pchain.getSuccOf(s); // get successor, whether s is <init> specialinvoke or not
	}
	private static Stmt getFirstSafeNonIdStmt(Value val, SootMethod m) {
		Body b = m.retrieveActiveBody();
		PatchingChain pchain = b.getUnits();
		Stmt s = UtilInstrum.getFirstNonIdStmt(pchain);
		if (m.getName().equals("<init>") && val == b.getLocals().getFirst()) {
			while (!(s.containsInvokeExpr() && s.getInvokeExpr() instanceof SpecialInvokeExpr && ((SpecialInvokeExpr)s.getInvokeExpr()).getMethod().getName().equals("<init>")
						&& ((SpecialInvokeExpr)s.getInvokeExpr()).getBase() == val))
				s = (Stmt) pchain.getSuccOf(s);
			s = (Stmt) pchain.getSuccOf(s);
		}
		return s;
	}

	private static boolean isParUsed(IdentityStmt s, NodeReachDefsUses nRU, SootMethod m) {
		Local lPar = (Local) ((IdentityStmt)s).getLeftOp();
		// ensure that formal param is used
		BitSet bsUIn = nRU.getUBackOut();
		CFGDefUses cfgDU = (CFGDefUses) ProgramFlowGraph.inst().getCFG(m);
		List<Use> uses = cfgDU.getUses();
		final int numUses = uses.size();
		for (int i = 0; i < numUses; ++i) {
			if (bsUIn.get(i) && uses.get(i).getValue() == lPar)
				return true;
		}
		return false;
	}
	
	private static final String LOCAL_OBJ = "<loc_obj>";
	private Value boxVar(SootMethod m, Variable v, List probe) {
		Body b = m.retrieveActiveBody();
		Value vFrom;
		if (v.isObject() && (vFrom = ((ObjVariable)v).getBaseLocal()) != null)
			;
		else
			vFrom = v.getValue();
		
		// const, local, field, array elem, obj w/o base local (i.e., static field or str const)
		Local lObj = UtilInstrum.getCreateLocal(b, LOCAL_OBJ, clsObject.getType());
		addCopyBoxOrCastStmt(b, probe, lObj, vFrom);
		return lObj;
	}
	
	private static final String LOCAL_BOX_PREFIX = "<loc_box_";
	private static final String LOCAL_BOX_SUFFIX = ">";
	private void addCopyBoxOrCastStmt(Body b, List probe, Local lObj, Value vFrom) {
		// box local into obj local, if primitive; just copy to obj local otherwise
		Type t = vFrom.getType();
		
		// for use in boxing ctor call or cast stmt, non-const non-local value must be copied to local first
		Value vFinalFrom;
		if (!(vFrom instanceof Constant || vFrom instanceof Local)) {
			// get/create local of appropriate type
			Local lValCopy = UtilInstrum.getCreateLocal(b, LOCAL_BOX_PREFIX+t+LOCAL_BOX_SUFFIX, t);
			Stmt sCopyToLocal = Jimple.v().newAssignStmt(lValCopy, vFrom);
			probe.add(sCopyToLocal);
			vFinalFrom = lValCopy; // replace vFrom
		}
		else
			vFinalFrom = vFrom;
		
		if (t instanceof PrimType) {
			Pair<RefType,SootMethod> refTypeAndCtor = Util.getBoxingTypeAndCtor((PrimType)t);
			Stmt sNewBox = Jimple.v().newAssignStmt(lObj, Jimple.v().newNewExpr(refTypeAndCtor.first()));
			Stmt sInitBox = Jimple.v().newInvokeStmt(Jimple.v().newSpecialInvokeExpr(lObj, refTypeAndCtor.second().makeRef(), vFinalFrom));
			probe.add(sNewBox);
			probe.add(sInitBox);
		}
		else {
			assert t instanceof RefLikeType;
			Stmt sCopyRef = Jimple.v().newAssignStmt(lObj, vFinalFrom);//Jimple.v().newCastExpr(vFinalFrom, clsObject.getType()));
			probe.add(sCopyRef);
		}
	}
	
}
