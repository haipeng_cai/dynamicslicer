package sli;

import java.util.ArrayList;
import java.util.List;

public class DynWSlicingOptions {
	private static List<Integer> startStmtIds = null;
	private static boolean fwdDynSliceInstr = false; // indicates forward dyn-slicing instrumentation instead of default exec-hist instrum
	private static boolean debugOut = false;
	private static int dist = -1; // default distance
	private static int backEdgeFreq = 4;
	private static boolean simpleLoops = false;
	private static boolean genAllToAll = false;
	private static boolean dynWSlice = false;
	
	private static String pathBase = "";
	private static String version; // including seed number, such as "v0s1" or "v1s2-orig"
	private static int expectedTestNum =100000; // by default, warning is restrained.
	private static int dyninstEvenLimit=-1; // by default, this is not limit of events during dynslice instrumentation
	
	public static List<Integer> getStartStmtIds() { return startStmtIds; }
	public static boolean fwdDynSliceInstr() { return fwdDynSliceInstr; }
	public static boolean debugOut() { return debugOut; }
	public static int dist() { return dist; }
	public static int backEdgeFreq() { return backEdgeFreq; }
	public static boolean simpleLoops() { return simpleLoops; }
	public static boolean genAllToAll() { return genAllToAll; }
	public static int eventLimit() { return dyninstEvenLimit; }
	
	public static boolean dynWSlice() {return dynWSlice;}
	public static String pathBase() { return pathBase; }
	public static String version() { return version; }
	public static int expectedTestNum() { return expectedTestNum; }
		
	public static String[] process(String[] args) {
		List<String> argsFiltered = new ArrayList<String>();
		
		boolean allowPhantom = true;
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];
			if (arg.startsWith("-start:")) {
				assert startStmtIds == null;
				startStmtIds = dua.util.Util.parseIntList(arg.substring("-start:".length()));
			}
			else if (arg.equals("-fdynslice"))
				fwdDynSliceInstr = true;
			else if (arg.equals("-debug"))
				debugOut = true;
			else if (arg.startsWith("-dist")) {
				dist = Integer.parseInt(args[++i]);
				assert dist >= 0;
			}
			else if (arg.equals("-nophantom"))
				allowPhantom = false;
			else if (arg.equals("-simpleloops"))
				simpleLoops = true;
			else if (arg.equals("-genalltoall"))
				genAllToAll = true;
			else if (arg.startsWith("-befreq:"))
				backEdgeFreq = Integer.valueOf(arg.substring("-befreq:".length()));
			else if (arg.equals("-dynWSlice"))
				dynWSlice = true;
			else if (arg.startsWith("-pathBase")) {
				pathBase = args[++i];
			}
			else if (arg.startsWith("-version")) {
				version = args[++i];
			}
			else if (arg.startsWith("-testNum")) {
				expectedTestNum = Integer.parseInt(args[++i]);
			}
			else if (arg.startsWith("-eventLimit")) {
				dyninstEvenLimit= Integer.parseInt(args[++i]);
			}
			else
				argsFiltered.add(arg);
		}
		
		if (allowPhantom)
			argsFiltered.add("-allowphantom");
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return argsFiltered.toArray(arrArgsFilt);
	}
	
}
