package sli;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.Dependence;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

/** Create a data structure for Execution History read from a given file, which is the execution output of the subject;
 *   Here EH is defined as a sequence of statement occurrences where each element is in the format of
 *   (Statement Id, rhsPos, occurrence number)
 */
public class ExecutionHistory
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static class EHNode {
		public int sid; // statement id
		public int rhsPos; // PRE_RHS or POST_RHS
		public int or; // order of occurrence
		
		EHNode() {
			sid = -1; // -1 indicates that this is unknown so far
			rhsPos = NodePoint.POST_RHS; // by default this is 1
			or = 1; // once this instance instantiated, the statement must have at least appeared once
		}
		public EHNode(int n, int rhsPos) { this.sid = n; this.rhsPos = rhsPos; this.or = 1; }
		public EHNode(int n, int rhsPos, int or) { this.sid = n; this.rhsPos = rhsPos; this.or = or; }
		@Override public int hashCode() { return sid << 16 | (int)or << 8 | rhsPos; } // to be finalized if necessary
		@Override public boolean equals(Object o) {
			return sid == ((EHNode)o).sid && rhsPos == ((EHNode)o).rhsPos && or == ((EHNode)o).or; 
		}
		@Override public String toString() { return "{"+sid+"["+rhsPos+"]}^" + or; }
		public boolean shallowEquals(Object o) { // without checking equality in the occurrence number
			return sid == ((EHNode)o).sid && rhsPos == ((EHNode)o).rhsPos; 
		}
		
		public boolean equalsToNodePoint(NodePoint o) { // without checking equality in the occurrence number
			if (null == o.getN().getStmt()) return false; // for special CFGNode, simply regarding it as unequal
			return sid == StmtMapper.getGlobalNodeId(o.getN()) && rhsPos == o.getRhsPos(); 
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private final static String DATA_PREFIX = "|||";
	private final static int DATA_PREFIX_LEN = DATA_PREFIX.length();
	
	private String inFile; // file where the history is to be loaded
	private int startId; // if given specifically, will only a partial execution history, e.g. the part relevant to this statement
	public Set<EHNode> stash; // the storage, a map from NodePoint to AEH record
	public Map<Integer, Integer> quickidx; // a quick indexing map: statement id -> number of occurrences
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	ExecutionHistory() { this.inFile = ""; startId = -1;}
	ExecutionHistory(String inFile) { this.inFile = inFile; startId = -1;}
	ExecutionHistory(String inFile, int startId) {	this.inFile = inFile; this.startId = startId;}
	public int getStart() { return startId; }
	public Set<EHNode> getStash() { return stash; }
	public Map<Integer, Integer> getQuickidx() { return quickidx; }
			
	public int LoadEH() {
		return LoadEH(this.inFile);
	}
	
	public int LoadEH(String inFile) {
		if (inFile.length() < 1) return -1;
		this.inFile = inFile;
		String infile = this.inFile;
		
		int startId = this.startId;
		boolean reachedStart = (-1 == startId);
		int lStmtCnt = 0;
		Integer nor = null;
		
		stash = new LinkedHashSet<EHNode>();
		quickidx = new LinkedHashMap<Integer, Integer>();
		
		try{
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(infile));
			String s;
			while ((s = reader.readLine()) != null) {
				if((!s.startsWith(DATA_PREFIX)) && (s.indexOf(DATA_PREFIX) != -1)){
					s = s.substring(s.indexOf(DATA_PREFIX));
				}
				// check if line is exec hist output line
				if (!s.startsWith(DATA_PREFIX)) {
					continue;
				}
				
				// parse point as integer value that is positive if followed by [1] and negative if followed by [0]
				final int bracketPos = s.indexOf('[', DATA_PREFIX_LEN);
				final int point = Integer.valueOf(s.substring(DATA_PREFIX_LEN, bracketPos));
				if (!reachedStart) {
					if (point == startId)
						reachedStart = true;
					else
						continue; // ignore data occurring BEFORE start point
				}
				final char sign = s.charAt(bracketPos + 1);
				assert sign == '0' || sign == '1';
				//final int signedPoint = sign == '1'? point : -point;
				assert s.charAt(bracketPos + 2) == ']' && s.charAt(bracketPos + 3) == '=';
				
				// store exec hist entry point->val
				nor = (Integer) quickidx.get(point);
				if ( null == nor ) {
					nor = 0;
				}
				nor++;
				quickidx.put(point, nor);
				
				stash.add(new EHNode(point, sign-'0', nor));
				
				lStmtCnt ++;
			}
			return 0;
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return -2; 
		}
		catch (IOException e) {
			throw new RuntimeException(e); 
		}
	}
	
	private static String removeFinalAddress(String s) {
		// remove object address, if it exists
		final int addrPos = s.lastIndexOf('@');
		if (addrPos != -1) {
			// now, ensure there is an hexadecimal number after the @
			boolean removeAddr = true;
			for (int i = addrPos + 1; i < s.length(); ++i) {
				final char c = s.charAt(i);
				if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'))) {
					removeAddr = false;
					break;
				}
			}
			if (removeAddr) {
				s = s.substring(0, addrPos + 1);
			}
		}
		return s;
	}
	
	/**
	 * Check if a dependence edge has been activated by examining if the src and target node have appeared as a
	 * continuous pair in the execution history
	 */
	public boolean isDependenceActivated(Dependence dep) {
		// quickly check existence of both endpoints firstly: either one's being special CFGNode inside is not considered further
		//if (null == dep.getSrc().getN().getStmt() || null == dep.getTgt().getN().getStmt()) return false;
		final int idsrc = StmtMapper.getGlobalNodeId(dep.getSrc().getN());
		final int idtgt = StmtMapper.getGlobalNodeId(dep.getTgt().getN());
		//if ( !(quickidx.containsKey(idsrc) && quickidx.containsKey(idtgt)) ) return false;
		boolean ret = (quickidx.containsKey(idsrc) && quickidx.containsKey(idtgt));
		if ( ret ) return true;
		
		// now check further for node pair with this dependence edge 
		// NOTE: for a dependence edge to be activated, the two endpoints do not have to appear consecutively in the execution history
		EHNode srcnode = new EHNode(idsrc, dep.getSrc().getRhsPos());
		EHNode tgtnode = new EHNode(idtgt, dep.getTgt().getRhsPos());
		
		ArrayList<EHNode> allEHNodes = new ArrayList<EHNode>(stash);
		int szStash = allEHNodes.size();
		/*
		for (int i = 0; i < szStash - 1; ++i) {
			if ( allEHNodes.get(i).shallowEquals(srcnode) && allEHNodes.get(i+1).shallowEquals(tgtnode) ) { 
				return true;
			}
		}
		*/
		
		int i;
		for (i = 0; i < szStash; ++i) {
			if ( allEHNodes.get(i).shallowEquals(srcnode)) { 
				break;
			}
		}
		if ( i >= szStash ) return false;
		
		for (i=0; i < szStash; ++i) {
			if ( allEHNodes.get(i).shallowEquals(tgtnode)) { 
				return true;
			}
		}
		
		return false;
	}

	// DEBUG PURPOSES
	public void dumpStash() {
		System.out.println("======================= All Records in EH Stash =====================");
		for (EHNode pntex : stash) {
			System.out.println("<" + pntex + ">");
		}
		System.out.println("============================================================");
	}
	
	public void dumpQuickidx() {
		Set<Integer> pnts = new LinkedHashSet<Integer>(quickidx.keySet());
		System.out.println("======================= All Records in EH Index ====================");
		for (Integer pnt : pnts) {
			System.out.println("(" + pnt + "," + (Integer)quickidx.get(pnt) + ")");
		}
		System.out.println("===========================================================");
	}
}

// the end
