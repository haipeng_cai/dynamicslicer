package sli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import options.Options;

import profile.DynSliceInstrumenter;
import profile.ExecHistInstrumenter;
import profile.StaticSliceReader;
import dua.Extension;
import dua.Forensics;
import dua.global.dep.DependenceFinder;
import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.DataDependence;
import dua.global.dep.DependenceFinder.ControlDependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

import util.DynSliceData;
import util.OutFileReader;
import profile.DynSliceExaminer.DepInst;

public class DynWSlicing implements Extension {
	private int totalTestRun = 0;  // actual number of test cases (output files), corresponding to the number of execution history files
	
	// the map: statementId -> distance (from the start node given by startId)
	private Map<Integer, Integer> nodeDistance = new LinkedHashMap<Integer, Integer>();
	
	// the map: statement id -> rank 
	private Map<Integer,Float> stmtRanking = new LinkedHashMap<Integer,Float>();
	
	private StaticSliceReader sliceReader = null;
	private int eventsLeft = StaticSliceReader.NO_LIMIT;
	private StaticSliceReader getCreateSliceFileReader() {
		if (sliceReader == null) {
			sliceReader = new StaticSliceReader();
			eventsLeft = sliceReader.getEventsLimit();
		}
		return sliceReader;
	}
	
	class depMapNodeComparator implements Comparator<Integer> {
		private depMapNodeComparator() {}
		public depMapNodeComparator v() { return new depMapNodeComparator(); }
		public int compare(Integer o1,Integer o2){
			Float v1=stmtRanking.get(o1), v2=stmtRanking.get(o2);
			if (v1<v2)
				return 1;
			if (v1>v2)
				return -1;
			return 0;
		}
	}
	
	public static void main(String[] args) {
		args = preProcessArgs(args);
		
		// certain parameters are mandatory for doing DynWSlicing
		if ( DynWSlicingOptions.dynWSlice() &&
			  (DynWSlicingOptions.pathBase().equalsIgnoreCase("") || DynWSlicingOptions.expectedTestNum() < 1) ) {
			System.err.println("Invalid or missing arguments for pathBase or expectedTestNum...");
			return;
		}
		
		DynWSlicing dynwSlicer = new DynWSlicing();
		
		// computer dynamic slice from execution history produced from running the dynsliceInstrumented subject with a test suite
		if (DynWSlicingOptions.dynWSlice()){
			dynwSlicer.retrieveDynSliceFromExecHist();
			return;
		}
		
		// just do dynwslice instrumentation
		Forensics.registerExtension(dynwSlicer);
		Forensics.main(args);
	}
	
	private static String[] preProcessArgs(String[] args) {
		args = DynWSlicingOptions.process(args);
		
		// add option to consider formal parameters as defs and return values as uses, for reaching def/use analysis
		//     and option to not remove repeated branches in switch nodes
		String[] args2 = new String[args.length + 2];
		System.arraycopy(args, 0, args2, 0, args.length);
		args2[args.length]     = "-paramdefuses";
		args2[args.length + 1] = "-keeprepbrs";
		
		args = args2;
		return args;
	}
	
	public void retrieveDynSliceFromExecHist() {
		if (0 != doDynWslicing()) {
			System.err.println("Error occurred during DynWSlicing, bailed out now.");
			return;
		}
		
		if (DynWSlicingOptions.debugOut()) {
			// dump the dynamic wslicing result 
			writeNodeDistance(this.nodeDistance,  dua.util.Util.getCreateBaseOutPath() + "wsliceFinal.out");
		}
		
		// Ranking statements according to each's distance from the given start node
		computeWSliceRanking();
		
		// save ranking result in the non-ascending order of statement ranks 
		List<Integer> sortedWSlicePnts = sortPointsByWSliRank(this.stmtRanking);
		try {
			File fwranking = new File(dua.util.Util.getCreateBaseOutPath() + "dynwsliceImpactRanking-" + 
					DynWSlicingOptions.version() + "-" + DynWSlicingOptions.getStartStmtIds().get(0));
			FileWriter fwriter = new FileWriter(fwranking);
			
			for (Integer pnt : sortedWSlicePnts) {
				fwriter.write(pnt + " : " + stmtRanking.get(pnt) + "\n");
			}
			
			// for those statements absent from the slice, we also rank them at the bottom
			Set<Integer> allStaticSlicePnts = new HashSet<Integer>();
			for (StaticSliceReader.RT_Dependence rdep:sliceReader.getDeps()) {
				allStaticSlicePnts.add(rdep.getSrc());
				allStaticSlicePnts.add(rdep.getTgt());
			}
			
			Set<Integer> dynSlicePnts = new HashSet<Integer>(sortedWSlicePnts);
			Float btmRank = (float) (dynSlicePnts.size() + (allStaticSlicePnts.size() - dynSlicePnts.size() + 1.0)*0.5);
			for(Integer pnt:allStaticSlicePnts) {
				if ( ! dynSlicePnts.contains(pnt) ) {
					fwriter.write(pnt + " : " + btmRank + "\n");
				}
			}
			
			fwriter.flush();
			fwriter.close();
		}
		catch (IOException e) { 
			e.printStackTrace(); 
		}
		
		return;
	}
		
	@Override public void run() {
		System.out.println("Running DynWSlicing extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();
		
		// for now, assume exactly one start point is provided
		CFGNode nStart = StmtMapper.getNodeFromGlobalId( DynWSlicingOptions.getStartStmtIds().get(0) );
		DependenceGraph depGraph = new DependenceGraph(
				new NodePoint(nStart, nStart.hasAppCallees()?NodePoint.PRE_RHS:NodePoint.POST_RHS), -1);
		
		if ( DynWSlicingOptions.dist() >= 1) {
			findDistPoints(depGraph);
		}
		
		if (DynWSlicingOptions.fwdDynSliceInstr()) {
			/**  just do DynSlice instrumentation, which is actually for the first step in dynamic slicing, i.e to produce
			 *   execution history on which DynWSlicing is based to proceed 
			 */
			System.out.println("Running DynSliceInstrumentation ...... includeVals=true, changeCovs=true, branchCov=false, DuaCov=false,"
					+ "eventlimit=" + DynWSlicingOptions.eventLimit());
			DynSliceInstrumenter instr = new DynSliceInstrumenter(true, true, false, false, DynWSlicingOptions.eventLimit());
			instr.instrument(depGraph);
			return;
		}
	
		/** if nothing particular specified, just do exechist instrumentation, 
		 *   which is actually NOT used for dynamic slicing by extracting 
		 *   execution history only
		 */ 
		System.out.println("Running ExecHistInstrumentation  ...... ");
		ExecHistInstrumenter instr = new ExecHistInstrumenter();
		instr.instrument(depGraph.getPointsInSlice());
	}
	
	private void buildDynSliceHelper(Set<DepInst> store, DynSliceData.DynFwdSlice dynSlice, DepInst startDepInst) {
		store.add(startDepInst);
		List<DepInst> successors = dynSlice.getSuccDepInsts(startDepInst);
		if (null == successors) return;
		
		for (DepInst depinst:successors) {
			if (!store.contains(depinst)) {
				buildDynSliceHelper(store, dynSlice, depinst);
			}
		}
	}
	
	private int doDynWslicing() {
		// read into the (forward) static slice info in slice.out, which is produced by DynSliceInstrumenter in the instrumentation phase
		this.getCreateSliceFileReader();
		
		// for now, assume exactly one start point is provided
		// NOTE: for dynamic slicing extraction, the startId is given as a signed statementId, including "minus" if necessary.
		Integer startId =  DynWSlicingOptions.getStartStmtIds().get(0);
		
		for (int tId = 1; true; ++tId) {
			String ehfile = DynWSlicingOptions.pathBase() + File.separator + tId + ".out";
			File ftest = new File(ehfile);
			if (!ftest.exists()) {
				System.out.println("File not exist, causing halt now: " + ehfile);
				break;
			}
			
			// read current execution history stored in output file given by ehfile and skip it if it does not even cover the start node specified
			DynSliceData dynSliceData = new DynSliceData(null, null);
			OutFileReader dslDataReader = new OutFileReader(DynWSlicingOptions.pathBase(), false);
			dynSliceData.parseDynSliceFromTestOut(sliceReader, dslDataReader, Integer.toString(tId), DynWSlicingOptions.getStartStmtIds());
			
			if (!dynSliceData.getChangesCov().contains(startId)) {
				System.out.println("Warning - current execution history output skipped due to its not covering the start node: " + ehfile);
				continue;
			}
			//System.out.println(dynSliceData.getChangesCov());
			
			// pick the execution history corresponding to the "change node" given by startId and 
			// then retrieve dynamic slice starting from that node  
			DynSliceData.ExecHistory execHist = dynSliceData.getChToExecHist().get(Math.abs(startId));
			assert null != execHist;
			
			// aggregate all dependence instances reachable from the start node
			Set<DepInst> allDepInstFromStart = new HashSet<DepInst>();
			for (DynSliceData.DynFwdSlice dynSlice : execHist.getSliceOccurrences()) {
				for (DepInst depinst:dynSlice.getRootDepInsts()) {
					if ( sliceReader.getDep(depinst.getDepId()).getSrc() != startId ) {
						continue;
					}

					buildDynSliceHelper(allDepInstFromStart, dynSlice, depinst);
				}
			}

			Set<StaticSliceReader.RT_Dependence> allActivatedDeps = new HashSet<StaticSliceReader.RT_Dependence>();
			int nActivated = 0;
			// build dependences from dependence instances
			for(DepInst depinst : allDepInstFromStart) {
				allActivatedDeps.add(sliceReader.getDep(depinst.getDepId()));
				nActivated ++;
			}
			
			System.out.println(nActivated + " dependence edges exercised via current execution history output in " + ehfile);
			
			// create a stmtId->outDepIds map for the purpose of depth computation by BFS traversal (hierarchical traversal)
			Map<Integer, Set<StaticSliceReader.RT_Dependence>> pntIdToDepIds = 
				new HashMap<Integer, Set<StaticSliceReader.RT_Dependence>>(); 
			for (StaticSliceReader.RT_Dependence rtdep:allActivatedDeps) {
				Integer src = rtdep.getSrc();
				Set<StaticSliceReader.RT_Dependence> outDeps = pntIdToDepIds.get(src);
				if (null == outDeps) {
					outDeps = new HashSet<StaticSliceReader.RT_Dependence>();
				}
				outDeps.add(rtdep);
				pntIdToDepIds.put(src, outDeps);
			}
			
			// if the start node is not on any activated dependence edges, no need to proceed either 
			if (pntIdToDepIds.get(startId) == null) {
				System.out.println("Warning - current execution history output skipped due to its not activating any edge on which the start node lies " + ehfile);
				continue;
			}
			
			//DEBUG
			if (DynWSlicingOptions.debugOut()) {
				System.out.println("The exercised Dependence-occurrence Graph rooted by the startId " + startId);
				dumpRootedDependenceOccurrenceGraph(pntIdToDepIds);
			}
			
			// compute depth : we force the successful completion for current execution history in order to proceed
			if ( 0 != computeDepth(pntIdToDepIds, startId, tId) ) {
				System.err.println("Distance updating with current execution history output in " + ehfile + "  failed; Halted now.");
				return -1;
			}
			
			this.totalTestRun ++;
			if (this.totalTestRun > DynWSlicingOptions.expectedTestNum()) {
				System.out.println("Reached the maximal number of test outputs: " + DynWSlicingOptions.expectedTestNum());
				break;
			}
		}
		
		System.out.println(this.totalTestRun + " execution history outputs have been processed.");
		
		return 0;
	}
	
	private void dumpRootedDependenceOccurrenceGraph(Map<Integer, Set<StaticSliceReader.RT_Dependence>> pntIdToDepIds) {
		for (Integer src:pntIdToDepIds.keySet()) {
			if (pntIdToDepIds.get(src).isEmpty()) continue;
			System.out.print("" + src + " --> ");
			int i = 0;
			for (StaticSliceReader.RT_Dependence dep:pntIdToDepIds.get(src)) {
				if ( i > 0 ) System.out.print(",");
				System.out.print("" + dep.getTgt());
				
				if (dep.isData()) System.out.print(":(d,varId=" + dep.getVarOrBrId() + ")");
				else System.out.print(":(c,brId=" + dep.getVarOrBrId() + ")");
				i++;
			}
			System.out.println("");
		}
	}
	
	/**
	 * Helper for doDynWslicing : hierarchical traversal over a dependence graph to compute 
	 * depth of each edge, and then minimal distance of each node, from the given start node  
	 */
	private int computeDepth(Map<Integer, Set<StaticSliceReader.RT_Dependence>> pntIdToDepIds, int startId, int tId) {
		// calculate dependence edge-wise depth information in the first place
		Map<StaticSliceReader.RT_Dependence, Integer> depDepths = new HashMap<StaticSliceReader.RT_Dependence,Integer>();
		int startDepth = 1;
		int parentDepth = 1;
		int childDepth = 0;
		
		// Do BFS on depGraph to find depth of each dep
		Queue<StaticSliceReader.RT_Dependence> queue = new LinkedList<StaticSliceReader.RT_Dependence>(); // Store dependence in BFS order
		Set<StaticSliceReader.RT_Dependence> startDep = pntIdToDepIds.get(startId);
		Set<StaticSliceReader.RT_Dependence> visited = new HashSet<StaticSliceReader.RT_Dependence>();
		for( StaticSliceReader.RT_Dependence dep : startDep){
			queue.add(dep);
			visited.add(dep);
			depDepths.put(dep, startDepth);
		}
		while(!queue.isEmpty()){
			StaticSliceReader.RT_Dependence dep = (StaticSliceReader.RT_Dependence)queue.remove();
			parentDepth = depDepths.get(dep);
			
			Set<StaticSliceReader.RT_Dependence> outDeps = pntIdToDepIds.get(dep.getTgt());
			if (null == outDeps) continue;
			for(StaticSliceReader.RT_Dependence outDep : outDeps){
				if(! visited.contains(outDep)){
					visited.add(outDep);
					queue.add(outDep);
					childDepth = parentDepth + 1;
					depDepths.put(outDep, childDepth);
				}
				else
					break;
			}
		}
		
		if (DynWSlicingOptions.debugOut()) {
			System.out.println("Calculting W-slice depth.........");
			writeMapToFile(depDepths, dua.util.Util.getCreateBaseOutPath() + "DepDepth" + tId + ".txt");
		}
		
		// now calculate statement-wise distance from the start node, map of statementId-->Depth
		Map<Integer,Integer> wsliceDepthFromStart = new HashMap<Integer,Integer>();

		// Iterate through depDepths map(<Dependence,Integer>), find target NodePoint for each Dependence, and assign the depth to each NodePont
		Iterator<Map.Entry<StaticSliceReader.RT_Dependence, Integer>> iter = depDepths.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry<StaticSliceReader.RT_Dependence, Integer> entry = (Map.Entry<StaticSliceReader.RT_Dependence, Integer>) iter.next();
			StaticSliceReader.RT_Dependence dependence = (StaticSliceReader.RT_Dependence) entry.getKey();
			Integer depth = (Integer) entry.getValue();
			Integer tgt = dependence.getTgt();
			
			//If the same tgt has been stored before, then compare the current depth and the stored depth and store the smaller one.
			if( wsliceDepthFromStart.containsKey(tgt)){
				Integer oldDepth =  wsliceDepthFromStart.get(tgt);
				if(oldDepth < depth)
					depth = oldDepth;
			}
			
			// global minimization of the distance
			if ( this.nodeDistance.containsKey(tgt) ) {
				Integer oldDepth = this.nodeDistance.get(tgt);
				if (oldDepth < depth) {
					depth = oldDepth;
				}
			}
			this.nodeDistance.put(tgt, depth);
			
			 wsliceDepthFromStart.put(tgt, depth);
		}
		
		if (DynWSlicingOptions.debugOut()) {
			// dump current wslicing output
			writeNodeDistance(wsliceDepthFromStart,  dua.util.Util.getCreateBaseOutPath() + "wslice" + tId + ".out");
		}
				
		return 0;
	}
	
	/**
	 * Helper for doDynWslicing : dump node->distance map  
	 */
	private void writeNodeDistance(Map<Integer,Integer> distmap, String filename) {
		File fOut = new File(filename);
		try {
			Writer writer = new FileWriter(fOut);
			
			List<Integer> pntsSorted = new ArrayList<Integer>(distmap.keySet());
			Collections.sort(pntsSorted);
			for (Integer pTgt : pntsSorted)
				writer.write(pTgt + "=" + distmap.get(pTgt) + '\n');
			
			writer.flush();
			writer.close();	
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	/** Looks for all points at distance d (given by setting). */
	private void findDistPoints(DependenceGraph depGraph) {
		final int d = DynWSlicingOptions.dist();
		
		Set<NodePoint> pntsAtDist = findNextDistPoints(depGraph, depGraph.getStart(), d);
		List<NodePoint> sortedPntsAtD = new ArrayList<NodePoint>(pntsAtDist);
		Collections.sort(sortedPntsAtD, NodePointComparator.inst);
		
		// DEBUG: print all points at dep distance d
		System.out.println("DISTANCE " + d + " points from " + StmtMapper.getGlobalNodeId(depGraph.getStart().getN()) + ": total " + sortedPntsAtD.size());
		for (NodePoint pntAtD : sortedPntsAtD)
			System.out.println(" " + StmtMapper.getGlobalNodeId(pntAtD.getN()));
	}
	/** Recursive helper for findDistPoints */
	private Set<NodePoint> findNextDistPoints(DependenceGraph depGraph, NodePoint pnt, int d) {
		assert d >= 1;
		
		Set<NodePoint> pntsAtDist = new HashSet<NodePoint>();
		if (d == 1) {
			// get immediate successors in dep graph
			for (Dependence dep : depGraph.getOutDeps(pnt))
				pntsAtDist.add(dep.getTgt());
		}
		else {
			// for each successor in dep graph, recursively advance with d-1
			for (Dependence dep : depGraph.getOutDeps(pnt))
				pntsAtDist.addAll( findNextDistPoints(depGraph, dep.getTgt(), d - 1) );
		}
		
		// DEBUG
		Set<NodePoint> dbgPntsSet = new HashSet<NodePoint>();
		for (Dependence dep : depGraph.getOutDeps(pnt))
			dbgPntsSet.add(dep.getTgt());
		List<NodePoint> dbgSortedPnts = new ArrayList<NodePoint>(dbgPntsSet);
		Collections.sort(dbgSortedPnts, NodePointComparator.inst);
		System.out.println(" d " + d + ": " + pnt + " -> " + dbgSortedPnts);
		
		return pntsAtDist;
	}
	
	/**
	 * Copied from TestAdequacy.sli.DuaProb.java
	 * @param duaIdProbs
	 * @param filepath
	 */
	public static void writeMapToFile(Map duaIdProbs, String filepath) {  
		try {
			String line = System.getProperty("line.separator");
			StringBuffer str = new StringBuffer();
			FileWriter fw = new FileWriter(filepath, false); //"C:\\dua\\ProbOutput\\StatDuaProb.txt"
			Set set = duaIdProbs.entrySet();
			Iterator iter = set.iterator();
			while(iter.hasNext()){
				Map.Entry entry = (Map.Entry)iter.next(); 
				str.append(entry.getKey()+" : "+entry.getValue()).append(line);
			}
			fw.write(str.toString());	
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Map<Integer,Float> computeWSliceRanking() {
		// ensure we access pnts in sorted order
		List<Integer> pntsSorted = new ArrayList<Integer>(this.nodeDistance.keySet());
		Collections.sort(pntsSorted);
		
		// compute inverse depth->pnts map
		Map<Integer, List<Integer>> depthToPnts = new HashMap<Integer, List<Integer>>();
		for (Integer pntTgt : pntsSorted) { 
			final Integer depth = this.nodeDistance.get(pntTgt);

			List<Integer> pntsForDepth = depthToPnts.get(depth);
			if (pntsForDepth == null) {
				pntsForDepth = new ArrayList<Integer>();
				depthToPnts.put(depth, pntsForDepth);
			}
			pntsForDepth.add(pntTgt);
		}
		
		// compute sorted list of depths, from LOWEST to HIGHEST
		List<Integer> depthsSorted = new ArrayList<Integer>(depthToPnts.keySet());
		Collections.sort(depthsSorted);
		
		// now, iterate over this sorted depths to assign rank to each point
		Float rank = 0.0f;
		Integer sizeCovered = 0;
		for (Integer depth : depthsSorted) {
			List<Integer> pnts = depthToPnts.get(depth);
			// rank for the tie elements is the middle value
			rank = sizeCovered + (pnts.size() + 1) * 0.5f;
			for (Integer pnt : pnts) {
				this.stmtRanking.put(pnt, rank);
			}
			// after computing and storing rank for this bunch of points, update "sizeCovered".
			sizeCovered += pnts.size();
		}
		return this.stmtRanking;
	}
	
	/** Right now, if two points have the same rank, sort them by integer order. */
	private static List<Integer> sortPointsByWSliRank(Map<Integer,Float> rankingWSlice) {
		List<Integer> sortedPnts = new ArrayList<Integer>();
		// create map rank->pnts
		Map<Float,List<Integer>> rankToPnts = new HashMap<Float, List<Integer>>();
		for (Integer pnt : rankingWSlice.keySet()) {
			Float rankOfPnt = rankingWSlice.get(pnt);
			// get create list of points for rank of current pnt
			List<Integer> pntsForRank = rankToPnts.get(rankOfPnt);
			if (pntsForRank == null) {
				pntsForRank = new ArrayList<Integer>();
				rankToPnts.put(rankOfPnt, pntsForRank);
			}
			pntsForRank.add(pnt);
		}
		
		for (float r = 0.5f; r < rankingWSlice.size(); r=r+0.5f) {
			List<Integer> pntsForRank = rankToPnts.get(r);
			if (pntsForRank == null)
				continue;
			Collections.sort(pntsForRank); // make it deterministic
			sortedPnts.addAll(pntsForRank);
		}
		
		return sortedPnts;
	}
}

