#!/bin/bash
# *** PARAMS   1=change 2=ver 3=seed
# *** EXAMPLE    1941    %2     s2-orig
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

ROOT=/home/hcai
subjectloc=$ROOT/SVNRepos/star-lab/trunk/Subjects/Schedule1/

INDIR=$subjectloc/DynwsliInstrumented-$ver-$seed-$change

#$ROOT/tools/DUAForensics-bins-code/DUAForensics
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin/:$ROOT/tools/java_cup.jar:$ROOT/workspace/DynWSlicing/bin:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$subjectloc/lib:$INDIR:$ROOT/workspace/TestAdequacy/bin"

OUTDIR=$subjectloc/dynwsli_outdyn-${ver}${seed}-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	prefix="\/home\/hcai\/SVNRepos\/star-lab\/trunk\/Subjects\/Schedule1"
	sed s'/\\/\//g' $subjectloc/inputs/testinputs.txt > $subjectloc/inputs/testinputs.mod

	cat $subjectloc/inputs/testinputs.mod | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo
		echo "Run Test #$i....."
		args=`echo $testname | sed s"/\.\./$prefix/"`
		java -Xmx4000m -ea -cp $MAINCP ScheduleClass $args 1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
	done
}

pushd . 1>/dev/null 2>&1
cd ${INDIR}
#cp $INDIR/slice.out ./
RunOneByOne
popd
#rm -f ./slice.out

stoptime=`date +%s%N | cut -b1-13`
echo "Dynamic W-Slicing Run Time for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0

