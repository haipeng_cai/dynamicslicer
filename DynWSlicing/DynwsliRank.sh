#!/bin/bash
#REM *** PARAMS   1=change 2=version 3=seed
#REM *** EXAMPLE    1941    v1        s2-orig

if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

ROOT=/home/hcai
subjectloc=$ROOT/SVNRepos/star-lab/trunk/Subjects/Schedule1/

#$ROOT/tools/DUAForensics-bins-code/DUAForensics
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin/:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/tools/java_cup.jar:$ROOT/workspace/DynWSlicing/bin:$ROOT/workspace/TestAdequacy/bin"

#$ROOT/tools/DUAForensics-bins-code/DUAForensics
SOOTCP=".:$ROOT/software/j2re1.4.2_18/lib/rt.jar:$ROOT/workspace/DUAForensics/bin/:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/workspace/Sensa/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/lib"

LOGDIR=$subjectloc/out-DynwsliRank
mkdir -p $LOGDIR

INDIR=$subjectloc/DynwsliInstrumented-$ver-$seed-$change
OUTDIR=$subjectloc/DynWslicingResults/
mkdir -p $OUTDIR

function Rank()
{
		#-debug \
		#-testNum 2650 \
	java -Xmx1600m -ea -cp ${MAINCP} sli.DynWSlicing \
		-start:$change \
		-dynWSlice \
		-version ${ver}${seed} \
		-pathBase $subjectloc/dynwsli_outdyn-${ver}${seed}-$change \
		1>$LOGDIR/dynwslice-$change-${ver}${seed}.out 2>$LOGDIR/dynwslice-$change-${ver}${seed}.err
}

pushd . 1>/dev/null 2>&1
cd ${INDIR}
starttime=`date +%s%N | cut -b1-13`
Rank
mv dynwsliceImpactRanking-$ver$seed-$change $OUTDIR/
mv wslice* DepDepth* $LOGDIR/
popd
stoptime=`date +%s%N | cut -b1-13`
echo "Dynamic W-Slicing Ranking Time for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0

