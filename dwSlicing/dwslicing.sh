#!/bin/bash
#REM *** PARAMS   1=change 2=version 3=seed
#REM *** EXAMPLE    1941    v1        s2-orig

if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

ROOT=/home/hcai
subjectloc=$ROOT/SVNRepos/star-lab/trunk/Subjects/Schedule1/

#$ROOT/tools/DUAForensics-bins-code/DUAForensics
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin/:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/tools/java_cup.jar:$ROOT/workspace/dwSlicing/bin"

#$ROOT/tools/DUAForensics-bins-code/DUAForensics
SOOTCP=".:$ROOT/software/j2re1.4.2_18/lib/rt.jar:$ROOT/workspace/DUAForensics/bin/:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/workspace/Sensa/bin:$subjectloc/bin/${ver}${seed}-orig:$subjectloc/lib"

mkdir -p out-dwSlice

INDIR=wsliceExechist_outdyn-${ver}${seed}-$change-orig
OUTDIR=$subjectloc/dwsli-$ver-$seed-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-testNum 2650 \
	#-pathBase "$subjectloc/dwsliTestOuputs" \
java -Xmx1600m -ea -cp ${MAINCP} sli.dwSlicing \
	-w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
	-p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
   	-duaverbose -start:$change \
	-allowphantom \
	-dwSlice \
	-pathBase "$INDIR/$change-observed/test" \
	-version "$ver$seed" \
	-slicectxinsens \
	-main-class ScheduleClass -entry:ScheduleClass \
	-process-dir $subjectloc/bin/${ver}${seed}-orig \
	1>out-dwSlice/dwslice-$change-${ver}${seed}.out 2>out-dwSlice/dwslice-$change-${ver}${seed}.err
stoptime=`date +%s%N | cut -b1-13`
echo "Dynamic W-Slicing Time for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "DWSlicing finished."
exit 0

