package sli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import profile.DynSliceInstrumenter;
import profile.ExecHistInstrumenter;
import dua.Extension;
import dua.Forensics;
import dua.global.dep.DependenceFinder;
import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.DataDependence;
import dua.global.dep.DependenceFinder.ControlDependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

import sli.ExecutionHistory;

public class dwSlicing implements Extension {
	private int totalTestRun = 0;  // actual number of test cases (output files), corresponding to the number of execution history files
	
	// the map: node point -> distance (from the start node given by startId)
	private Map<NodePoint, Integer> nodeDistance = new LinkedHashMap<NodePoint, Integer>();
	
	// the map: statement id -> rank 
	private Map<Integer,Float> stmtRanking = new LinkedHashMap<Integer,Float>(); 
	
	class depMapNodeComparator implements Comparator<Integer> {
		private depMapNodeComparator() {}
		public depMapNodeComparator v() { return new depMapNodeComparator(); }
		public int compare(Integer o1,Integer o2){
			Float v1=stmtRanking.get(o1), v2=stmtRanking.get(o2);
			if (v1<v2)
				return 1;
			if (v1>v2)
				return -1;
			return 0;
		}
	}
	
	public static void main(String[] args) {
		args = preProcessArgs(args);
		
		// certain parameters are mandatory for doing dwslicing
		if ( dwSlicingOptions.dwSlice() &&
			  (dwSlicingOptions.pathBase().equalsIgnoreCase("") || dwSlicingOptions.expectedTestNum() < 1) ) {
			System.err.println("Invalid or missing arguments for pathBase or expectedTestNum...");
			return;
		}
		
		dwSlicing probSlicer = new dwSlicing();
		Forensics.registerExtension(probSlicer);
		Forensics.main(args);
	}
	
	private static String[] preProcessArgs(String[] args) {
		args = dwSlicingOptions.process(args);
		
		// add option to consider formal parameters as defs and return values as uses, for reaching def/use analysis
		//     and option to not remove repeated branches in switch nodes
		String[] args2 = new String[args.length + 2];
		System.arraycopy(args, 0, args2, 0, args.length);
		args2[args.length]     = "-paramdefuses";
		args2[args.length + 1] = "-keeprepbrs";
		
		args = args2;
		return args;
	}
	
	@Override public void run() {
		System.out.println("Running dwSlicing extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();
		
		// for now, assume exactly one start point is provided
		CFGNode nStart = StmtMapper.getNodeFromGlobalId( dwSlicingOptions.getStartStmtIds().get(0) );
		DependenceGraph depGraph = new DependenceGraph(new NodePoint(nStart, NodePoint.POST_RHS), -1);
		
		//DEBUG
		if (dwSlicingOptions.debugOut()) {
			System.out.println("All Dependence Edges in the original static DependenceGraph");
			for (Dependence dep:depGraph.getDeps()) {
				System.out.println(dep);
			}
		}
		
		if ( dwSlicingOptions.dist() >= 1) {
			findDistPoints(depGraph);
		}
		
		if (dwSlicingOptions.fwdDynSliceInstr()) {
			DynSliceInstrumenter dynSliceInstrum = new DynSliceInstrumenter(false);
			dynSliceInstrum.instrument(depGraph);
		}

		if (dwSlicingOptions.dwSlice()){
			if (0 != doDynaWslicing(depGraph)) {
				System.err.println("Error occurred during DWSlicing, bailed out now.");
				return;
			}
			
			if (dwSlicingOptions.debugOut()) {
				// dump the dynamic wslicing result 
				writeNodeDistance(this.nodeDistance,  dua.util.Util.getCreateBaseOutPath() + "wsliceFinal.out");
			}
			
			// Ranking statements according to each's distance from the given start node
			computeWSliceRanking();
			
			// save ranking result in the non-ascending order of statement ranks 
			List<Integer> sortedWSlicePnts = sortPointsByWSliRank(this.stmtRanking);
			try {
				File fwranking = new File(dua.util.Util.getCreateBaseOutPath() + "dwsliceImpactRanking-" + 
						dwSlicingOptions.version() + "-" + dwSlicingOptions.getStartStmtIds().get(0));
				FileWriter fwriter = new FileWriter(fwranking);
				
				for (Integer pnt : sortedWSlicePnts) {
					fwriter.write(pnt + " : " + stmtRanking.get(pnt) + "\n");
				}
				
				fwriter.flush();
				fwriter.close();
			}
			catch (IOException e) { 
				e.printStackTrace(); 
			}
			
			return;
		}
		
		/** if not to do dwslicing, just do exechist instrumentation, which is actually for the first step in dynamic slicing, i.e to produce
		 *   execution history on which dwslicing is based to proceed 
		 */ 
		ExecHistInstrumenter instr = new ExecHistInstrumenter();
		instr.instrument(depGraph.getPointsInSlice());
	}
	
	private int doDynaWslicing(DependenceGraph depGraph) {
		int nDeps = depGraph.getDeps().size();
		
		for (int tId = 1; true; ++tId) {
			try {
				String ehfile = dwSlicingOptions.pathBase() + tId + "/run1.out";
				//ExecutionHistory cureh = new ExecutionHistory(ehfile, dwSlicingOptions.getStartStmtIds().get(0));
				ExecutionHistory cureh = new ExecutionHistory(ehfile);
				if (-2 == cureh.LoadEH() ) throw new FileNotFoundException();
				
				if (dwSlicingOptions.debugOut()) {
					cureh.dumpStash();
					cureh.dumpQuickidx();
				}
				
				// if the given start node is not even executed with the test input corresponding to current execution history, it would be
				// pointless to proceed for this execution history (waste of effort since it would not contribute in updating the distance map
				if ( cureh.getStash().isEmpty() ) {
					System.out.println("Warning - current execution history output skipped due to its not covering the start node: " + ehfile);
					continue;
				}
			
				// remove those dependence edges that have not exercised/activated by the test input corresponding to current 
				// execution history
				Set<Dependence> allActivatedDeps = new HashSet<Dependence>();
				int nActivated = 0;
				for (Dependence dep:depGraph.getDeps()) {
					if (cureh.isDependenceActivated(dep)) 	{
						/// !!! Shallow copy (by simply "allActivatedDeps.add(dep);" will cause the "visited" flag to stop traverse in computeDepth 
						/// for the second time and afterwards correctly
						if (dep instanceof DataDependence) {
							DataDependence ddep = (DataDependence)dep;
							allActivatedDeps.add( new DataDependence(ddep.getSrc(), ddep.getTgt(), ddep.getVar(), ddep.getType()));
						}
						else {
							ControlDependence cdep = (ControlDependence)dep;
							allActivatedDeps.add(new ControlDependence(cdep.getSrc(), cdep.getBrId(), cdep.getTgt()));
						}
						nActivated ++;
					}
				}
				
				System.out.println(nActivated + "/" + nDeps + " edges exercised via current execution history output in " + ehfile);
				
				// create a node->outDeps map for the purpose of depth computation by BFS traversal (hierarchical traversal)
				Map<NodePoint, Set<Dependence>> pntToDeps = new HashMap<NodePoint, Set<Dependence>>(); 
				for (Dependence dep:allActivatedDeps) {
					NodePoint src = dep.getSrc();
					Set<Dependence> outDeps = pntToDeps.get(src);
					if (null == outDeps) {
						outDeps = new HashSet<Dependence>();
					}
					outDeps.add(dep);
					pntToDeps.put(src, outDeps);
				}
				
				// if the start node is not on any activated dependence edges, no need to proceed either 
				if (pntToDeps.get(depGraph.getStart()) == null) {
					System.out.println("Warning - current execution history output skipped due to its not activating any edge on which the start node lies " + ehfile);
					continue;
				}
				
				//DEBUG
				if (dwSlicingOptions.debugOut()) {
					System.out.println("All Dependences in the exercised DependenceGraph");
					for (NodePoint src:pntToDeps.keySet()) {
						if (pntToDeps.get(src).isEmpty()) continue;
						System.out.print("" + src + " --> ");
						int i = 0;
						for (Dependence dep:pntToDeps.get(src)) {
							if ( i > 0 ) System.out.print(",");
							System.out.print("" + dep.getTgt());
							
							if (dep instanceof DataDependence) System.out.print(":(d,var=" + ((DataDependence)dep).getVar() + ")");
							else System.out.print(":(c,brid=" + ((ControlDependence)dep).getBrId() + ")");
							
							i++;
						}
						System.out.println("");
					}
				}
				
				// compute depth : we force the successful completion for current execution history in order to proceed
				if ( 0 != computeDepth(pntToDeps, depGraph.getStart(), tId) ) {
					System.err.println("Distance updating with current execution history output in " + ehfile + "  failed; Halted now.");
					return -1;
				}
			}
			catch (FileNotFoundException e) {
				if ( tId < dwSlicingOptions.expectedTestNum() + 1 ) {
					System.err.println(dwSlicingOptions.pathBase() + tId + "/1.out" + " was not found.");
				}
				break;
			}
			catch (IOException e) {
				throw new RuntimeException(e); 
			}
			
			this.totalTestRun ++;
		}
		
		System.err.println(this.totalTestRun + " execution history outputs have been processed.");
		
		return 0;
	}
	
	/**
	 * Helper for doDynaWslicing : hierarchical traversal over a dependence graph to compute 
	 * depth of each edge, and then minimal distance of each node, from the given start node  
	 */
	private int computeDepth(Map<NodePoint, Set<Dependence>> pntToDeps, NodePoint startPnt, int tId) {
		// calculate dependence edge-wise depth information in the first place
		Map<Dependence, Integer> depDepths = new HashMap<Dependence,Integer>();
		int startDepth = 1;
		int parentDepth = 1;
		int childDepth = 0;
		
		// Do BFS on depGraph to find depth of each dep
		Queue<Dependence> queue = new LinkedList<Dependence>(); // Store dependence in BFS order
		Set<Dependence> startDep = pntToDeps.get(startPnt);
		for( Dependence dep : startDep){
			queue.add(dep);
			dep.visited = true;
			depDepths.put(dep, startDepth);
		}
		while(!queue.isEmpty()){
			Dependence dep = (Dependence)queue.remove();
			parentDepth = depDepths.get(dep);
			
			Set<Dependence> outDeps = pntToDeps.get(dep.getTgt());
			if (null == outDeps) continue;
			for(Dependence outDep : outDeps){
				if(outDep != null && outDep.visited != true){
					outDep.visited = true;
					queue.add(outDep);
					childDepth = parentDepth + 1;
					depDepths.put(outDep, childDepth);
				}
				else
					break;
			}
		}
		
		// now calculate node-wise distance from the start node
		Map<NodePoint,Integer> wsliceDepthFromStart = new HashMap<NodePoint,Integer>();
		if (dwSlicingOptions.debugOut()) {
			System.out.println("Calculting W-slice depth.........");
			writeMapToFile(depDepths, dua.util.Util.getCreateBaseOutPath() + "DepDepth" + tId + ".txt");
		}

		// Iterate through depDepths map(<Dependence,Integer>), find target NodePoint for each Dependence, and assign the depth to each NodePont
		Iterator<Map.Entry<Dependence, Integer>> iter = depDepths.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry<Dependence, Integer> entry = (Map.Entry<Dependence, Integer>) iter.next();
			Dependence dependence = (Dependence) entry.getKey();
			Integer depth = (Integer) entry.getValue();
			NodePoint tgt = dependence.getTgt();
			
			//If the same tgt has been stored before, then compare the current depth and the stored depth and store the smaller one.
			if( wsliceDepthFromStart.containsKey(tgt)){
				Integer oldDepth =  wsliceDepthFromStart.get(tgt);
				if(oldDepth < depth)
					depth = oldDepth;
			}
			
			// global minimization of the distance
			if ( this.nodeDistance.containsKey(tgt) ) {
				Integer oldDepth = this.nodeDistance.get(tgt);
				if (oldDepth < depth) {
					depth = oldDepth;
				}
			}
			this.nodeDistance.put(tgt, depth);
			
			 wsliceDepthFromStart.put(tgt, depth);
		}
		
		if (dwSlicingOptions.debugOut()) {
			// dump current wslicing output
			writeNodeDistance(wsliceDepthFromStart,  dua.util.Util.getCreateBaseOutPath() + "wslice" + tId + ".out");
		}
				
		return 0;
	}
	
	/**
	 * Helper for doDynaWslicing : dump node->distance map  
	 */
	private void writeNodeDistance(Map<NodePoint,Integer> distmap, String filename) {
		File fOut = new File(filename);
		try {
			Writer writer = new FileWriter(fOut);
			
			List<NodePoint> pntsSorted = new ArrayList<NodePoint>(distmap.keySet());
			Collections.sort(pntsSorted, NodePointComparator.inst);
			for (NodePoint pTgt : pntsSorted)
				writer.write(pTgt + "=" + distmap.get(pTgt) + '\n');
			
			writer.flush();
			writer.close();	
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	/** Looks for all points at distance d (given by setting). */
	private void findDistPoints(DependenceGraph depGraph) {
		final int d = dwSlicingOptions.dist();
		
		Set<NodePoint> pntsAtDist = findNextDistPoints(depGraph, depGraph.getStart(), d);
		List<NodePoint> sortedPntsAtD = new ArrayList<NodePoint>(pntsAtDist);
		Collections.sort(sortedPntsAtD, NodePointComparator.inst);
		
		// DEBUG: print all points at dep distance d
		System.out.println("DISTANCE " + d + " points from " + StmtMapper.getGlobalNodeId(depGraph.getStart().getN()) + ": total " + sortedPntsAtD.size());
		for (NodePoint pntAtD : sortedPntsAtD)
			System.out.println(" " + StmtMapper.getGlobalNodeId(pntAtD.getN()));
	}
	/** Recursive helper for findDistPoints */
	private Set<NodePoint> findNextDistPoints(DependenceGraph depGraph, NodePoint pnt, int d) {
		assert d >= 1;
		
		Set<NodePoint> pntsAtDist = new HashSet<NodePoint>();
		if (d == 1) {
			// get immediate successors in dep graph
			for (Dependence dep : depGraph.getOutDeps(pnt))
				pntsAtDist.add(dep.getTgt());
		}
		else {
			// for each successor in dep graph, recursively advance with d-1
			for (Dependence dep : depGraph.getOutDeps(pnt))
				pntsAtDist.addAll( findNextDistPoints(depGraph, dep.getTgt(), d - 1) );
		}
		
		// DEBUG
		Set<NodePoint> dbgPntsSet = new HashSet<NodePoint>();
		for (Dependence dep : depGraph.getOutDeps(pnt))
			dbgPntsSet.add(dep.getTgt());
		List<NodePoint> dbgSortedPnts = new ArrayList<NodePoint>(dbgPntsSet);
		Collections.sort(dbgSortedPnts, NodePointComparator.inst);
		System.out.println(" d " + d + ": " + pnt + " -> " + dbgSortedPnts);
		
		return pntsAtDist;
	}
	
	/**
	 * Copied from TestAdequacy.sli.DuaProb.java
	 * @param duaIdProbs
	 * @param filepath
	 */
	public static void writeMapToFile(Map duaIdProbs, String filepath) {  
		try {
			String line = System.getProperty("line.separator");
			StringBuffer str = new StringBuffer();
			FileWriter fw = new FileWriter(filepath, false); //"C:\\dua\\ProbOutput\\StatDuaProb.txt"
			Set set = duaIdProbs.entrySet();
			Iterator iter = set.iterator();
			while(iter.hasNext()){
				Map.Entry entry = (Map.Entry)iter.next(); 
				str.append(entry.getKey()+" : "+entry.getValue()).append(line);
			}
			fw.write(str.toString());	
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Map<Integer,Float> computeWSliceRanking() {
		// ensure we access pnts in sorted order
		List<NodePoint> pntsSorted = new ArrayList<NodePoint>(this.nodeDistance.keySet());
		Collections.sort(pntsSorted, NodePoint.NodePointComparator.inst);
		
		// compute inverse depth->pnts map
		Map<Integer, List<Integer>> depthToPnts = new HashMap<Integer, List<Integer>>();
		for (NodePoint pntTgt : pntsSorted) { 
			final Integer depth = this.nodeDistance.get(pntTgt);

			List<Integer> pntsForDepth = depthToPnts.get(depth);
			if (pntsForDepth == null) {
				pntsForDepth = new ArrayList<Integer>();
				depthToPnts.put(depth, pntsForDepth);
			}
			pntsForDepth.add(StmtMapper.getGlobalNodeId(pntTgt.getN()));
		}
		
		// compute sorted list of depths, from LOWEST to HIGHEST
		List<Integer> depthsSorted = new ArrayList<Integer>(depthToPnts.keySet());
		Collections.sort(depthsSorted);
		
		// now, iterate over this sorted depths to assign rank to each point
		Float rank = 0.0f;
		Integer sizeCovered = 0;
		for (Integer depth : depthsSorted) {
			List<Integer> pnts = depthToPnts.get(depth);
			// rank for the tie elements is the middle value
			rank = sizeCovered + (pnts.size() + 1) * 0.5f;
			for (Integer pnt : pnts) {
				this.stmtRanking.put(pnt, rank);
			}
			// after computing and storing rank for this bunch of points, update "sizeCovered".
			sizeCovered += pnts.size();
		}
		return this.stmtRanking;
	}
	
	/** Right now, if two points have the same rank, sort them by integer order. */
	private static List<Integer> sortPointsByWSliRank(Map<Integer,Float> rankingWSlice) {
		List<Integer> sortedPnts = new ArrayList<Integer>();
		// create map rank->pnts
		Map<Float,List<Integer>> rankToPnts = new HashMap<Float, List<Integer>>();
		for (Integer pnt : rankingWSlice.keySet()) {
			Float rankOfPnt = rankingWSlice.get(pnt);
			// get create list of points for rank of current pnt
			List<Integer> pntsForRank = rankToPnts.get(rankOfPnt);
			if (pntsForRank == null) {
				pntsForRank = new ArrayList<Integer>();
				rankToPnts.put(rankOfPnt, pntsForRank);
			}
			pntsForRank.add(pnt);
		}
		
		for (float r = 0.5f; r < rankingWSlice.size(); r=r+0.5f) {
			List<Integer> pntsForRank = rankToPnts.get(r);
			if (pntsForRank == null)
				continue;
			Collections.sort(pntsForRank); // make it deterministic
			sortedPnts.addAll(pntsForRank);
		}
		
		return sortedPnts;
	}
}

